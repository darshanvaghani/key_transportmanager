﻿using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TransportManager.BLL.Service
{
    public class UserService : IUser
    {
        UserRepository userRepository = null;

        public Task<Tuple<LoginResponseModel, TranStatus>> CheckLoginUser(LoginRequestModel model)
        {
            using (userRepository = new UserRepository())
            {
                return userRepository.CheckLoginUser(model);
            }
        }

        public Task<Tuple<LoginResponseModel, TranStatus>> Register(RegisterModel model)
        {
            using (userRepository = new UserRepository())
            {
                return userRepository.Register(model);
            }
        }

        public Task<TranStatus> UpdateUser(UpdateUserModel model, long userId)
        {
            using (userRepository = new UserRepository())
            {
                return userRepository.UpdateUser(model, userId);
            }
        }

        public Task<TranStatus> Logout(long userId)
        {
            using (userRepository = new UserRepository())
            {
                return userRepository.Logout(userId);
            }
        }

        public async Task<TranStatus> ChangePassword(long userId, ChangePasswordRequestModel changePassword)
        {
            using (userRepository = new UserRepository())
            {
                TranStatus tranStatus = new TranStatus();
                return await userRepository.ChangePassword(userId, changePassword);
            }
        }

        public async Task<TranStatus> ChangeStatus(long userId, ChangeStatusModel model)
        {
            using (userRepository = new UserRepository())
            {
                TranStatus tranStatus = new TranStatus();
                return await userRepository.ChangeStatus(userId, model);
            }
        }

        public async Task<List<LoginHistoryResponseModel>> GetLoginHistory(long id)
        {
            using (userRepository = new UserRepository())
            {
                return await userRepository.GetLoginHistory(id);
            }
        }

        public async Task<TranStatus> Delete(long userId)
        {
            using (userRepository = new UserRepository())
            {
                return await userRepository.Delete(userId);
            }
        }

        public async Task<List<UserResponseModel>> GetUser(long id)
        {
            using (userRepository = new UserRepository())
            {
                return await userRepository.GetUser(id);
            }
        }
        
        public async Task<IList<UserRole>> GetRole()
        {
            using (userRepository = new UserRepository())
            {
                return await userRepository.GetRole();
            }
        }

    }
}
