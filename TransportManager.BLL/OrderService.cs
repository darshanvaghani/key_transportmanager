﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;

namespace TransportManager.BLL
{
	public class OrderService : IOrder
	{
		OrderRepository repository = null;

		public async Task<Tuple<List<GetOrderSummaryModel>, int>> GetOrderAsync(OrderRequestModel requestModel)
		{
			using (repository = new OrderRepository())
			{
				return await repository.GetOrderAsync(requestModel);
			}
		}

		public async Task<TranStatus> AddUpdateOrderAsync(AddOrderSummaryModel model)
		{
			using (repository = new OrderRepository())
			{
				return await repository.AddUpdateOrderAsync(model);
			}
		}

		public async Task<TranStatus> DeleteOrderAsync(DeleteModel model)
		{
			using (repository = new OrderRepository())
			{
				return await repository.DeleteOrderAsync(model);
			}
		}

		public async Task<Tuple<List<GetOrderSummaryModel>, int>> GetOrderByUserIdAsync(GetOrderByUserIdRequestModel requestModel)
		{
			using (repository = new OrderRepository())
			{
				return await repository.GetOrderByUserIdAsync(requestModel);
			}
		}

		public async Task<Tuple<List<GetOrderSummaryModel>, int>> GetRunningOrderByUserId(GetOrderByUserIdRequestModel requestModel)
		{
			using (repository = new OrderRepository())
			{
				return await repository.GetRunningOrderByUserId(requestModel);
			}
		}

		public async Task<Tuple<List<GetOrderSummaryModel>, int>> GetFeedForVehicleAsync(FeedSearchModel model)
		{
			using (repository = new OrderRepository())
			{
				return await repository.GetFeedForVehicleAsync(model);
			}
		}

		public async Task<TranStatus> ChangeOrderStatus(int id, ChangeStatusModel model)
		{
			using (repository = new OrderRepository())
			{
				return await repository.ChangeOrderStatus(id, model);
			}
		}
		public async Task<List<GetProductMaterialModel>> GetProductMaterial()
		{
			using (repository = new OrderRepository())
			{
				return await repository.GetProductMaterial();
			}
		}

		public async Task<TranStatus> StatusUpdate(StatusModel model)
		{
			using (repository = new OrderRepository())
			{
				return await repository.StatusUpdate(model);
			}
		}
		public async Task<TranStatus> ConfirmProposalInOrder(ConfirmProposalId model)
		{
			using (repository = new OrderRepository())
			{
				return await repository.ConfirmProposalInOrder(model);
			}
		}

	}
}
