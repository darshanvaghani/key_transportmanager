﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;

namespace TransportManager.BLL
{
    public class VehicleOrderService : IVehicleOrder
    {
        VehicleOrderRepository repository = null;
      
        public async Task<TranStatus> AddUpdateVehicleOrder(VehicleOrderSummaryModel model)
        {
            using (repository = new VehicleOrderRepository())
            {
                return await repository.AddUpdateVehicleOrder(model);
            }
        }

        public async Task<TranStatus> DeleteVehicleOrder(DeleteModel model)
        {
            using (repository = new VehicleOrderRepository())
            {
                return await repository.DeleteVehicleOrder(model);
            }
        }
        
        public async Task<TranStatus> ChangeVehicleOrderStatus(int id, ChangeStatusModel model)
        {
            using (repository = new VehicleOrderRepository())
            {
                return await repository.ChangeVehicleOrderStatus(id, model);
            }
        }

        public async Task<Tuple<List<VehicleOrderSummaryDetails>, int>> GetVehicleOrder(GetVehicleOrder model)
        {
            using (repository = new VehicleOrderRepository())
            {
                return await repository.GetVehicleOrder(model);
            }
        }

        public async Task<List<VehicleOrderSummaryDetails>> GetVehicleOrderById(GetVehicleOrder model)
        {
            using (repository = new VehicleOrderRepository())
            {
                return await repository.GetVehicleOrderById(model);
            }
        }

        public async Task<Tuple<List<VehicleOrderSummaryDetails>, int>> GetVehicleOrderByUserId(GetVehicleOrder model)
        {
            using (repository = new VehicleOrderRepository())
            {
                return await repository.GetVehicleOrderByUserId(model);
            }
        }

        public async Task<TranStatus> StatusUpdate(VehicleOrderStatusUpdateModel model)
        {
            using (repository = new VehicleOrderRepository())
            {
                return await repository.VehicleOrderStatusUpdate(model);
            }
        }

        public async Task<TranStatus> ConfirmProposalInVehicleOrder(ConfirmProposalIdInVehicleOrder model)
        {
            using (repository = new VehicleOrderRepository())
            {
                return await repository.ConfirmProposalInVehicleOrder(model);
            }
        }

    }
}
