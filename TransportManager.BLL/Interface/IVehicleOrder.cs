﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
	public interface IVehicleOrder
	{
		Task<TranStatus> AddUpdateVehicleOrder(VehicleOrderSummaryModel model);

		Task<TranStatus> DeleteVehicleOrder(DeleteModel model);

		Task<TranStatus> ChangeVehicleOrderStatus(int id, ChangeStatusModel model);

		Task<Tuple<List<VehicleOrderSummaryDetails>, int>> GetVehicleOrder(GetVehicleOrder model);

		Task<List<VehicleOrderSummaryDetails>> GetVehicleOrderById(GetVehicleOrder model);

		Task<Tuple<List<VehicleOrderSummaryDetails>, int>> GetVehicleOrderByUserId(GetVehicleOrder model);
        Task<TranStatus> StatusUpdate(VehicleOrderStatusUpdateModel model);
        Task<TranStatus> ConfirmProposalInVehicleOrder(ConfirmProposalIdInVehicleOrder model);
    }
}
