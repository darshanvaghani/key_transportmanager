﻿using Microsoft.Extensions.Primitives;

namespace TransportManager.BLL.Interface
{
	public interface IToken
    { 
        bool ValidateToken(string token);
    }
}
