﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
    public interface IVehicle
    {
        Task<TranStatus> AddVehicleType(VehicleTypeRequestModel model);
        Task<TranStatus> UpdateVehicleType(int id, UpdateVehicleTypeRequestModel model);
        Task<TranStatus> AddVehicle(VehicleModel model, IList<DocumentList> documentLists);
        Task<TranStatus> UpdateVehicle(int id, UpdateVehicleModel model, IList<DocumentList> documentLists);
        Task<TranStatus> DeleteVehicleType(int id, int createdBy);
        Task<TranStatus> DeleteVehicle(int id, int createdBy);
        Task<TranStatus> ChangeVehicleStatus(int id, ChangeStatusModel model);
        Task<IList<GetVehicleTypeModel>> GetVehicleType();
        Task<Tuple<List<GetVehicleModel>, int>> GetVehicle(GetVehicle model);
        Task<Tuple<List<GetVehicleModel>, int>> GetVehicleByUserId(GetVehicle model);
    }
}
