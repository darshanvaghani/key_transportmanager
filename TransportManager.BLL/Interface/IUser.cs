﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
    public interface IUser
    {
        Task<Tuple<LoginResponseModel, TranStatus>> CheckLoginUser(LoginRequestModel model);
        Task<Tuple<LoginResponseModel, TranStatus>> Register(RegisterModel model);
        Task<TranStatus> UpdateUser(UpdateUserModel model, long userId);
        Task<TranStatus> Logout(long userId);
        Task<TranStatus> ChangePassword(long userId, ChangePasswordRequestModel changePassword);
        Task<TranStatus> ChangeStatus(long userId, ChangeStatusModel model);
        Task<List<LoginHistoryResponseModel>> GetLoginHistory(long id);
        Task<TranStatus> Delete(long userId);
        Task<List<UserResponseModel>> GetUser(long id);
        Task<IList<UserRole>> GetRole();
    }
}
