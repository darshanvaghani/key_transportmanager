﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
	public interface IUserProposal
    {
		Task<TranStatus> AddUserProposal(UserProposalModel model);
		Task<TranStatus> UpdateUserProposal(int id, UpdateUserProposalModel model);
		Task<TranStatus> DeleteUserProposal(int id, int createdBy);
		Task<TranStatus> ChangeUserProposalStatus(ChangeUserProposalStatusModel model);
		Task<TranStatus> UpdateUserProposalStatus(UpdateUserProposalStatus model);
		Task<Tuple<List<GetUserProposalModel>, int>> GetAllUserProposal(PaginationModel model);
		Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalByUserId(GetUserProposalByUserId model);
		Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalById(GetUserProposalById model);
		Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalByVehicleOrderId(GetUserProposalByVehicleOrderId model);
	}
}
