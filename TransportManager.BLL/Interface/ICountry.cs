﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
	public interface ICountry
    {
        Task<List<CountryResponceModel>> GetCountryAsync();
        //TranStatus AddCountry(CountryModel itemModel);
        //TranStatus UpdateCountry(CountryModel itemModel);
        //TranStatus DeleteCountry(CountryModel itemModel);
    }
}
