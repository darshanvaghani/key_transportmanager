﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
	public interface IState
	{
        Task<List<StateResponceModel>> GetState(StateRequestModel itemModel);
		//IList<GetStateDropdown_Result> GetStateDropdown(out TranStatus transaction, int companyId);
  //      TranStatus AddState(StateModel itemModel);
  //      TranStatus UpdateState(StateModel itemModel);
  //      TranStatus DeleteState(StateModel itemModel);
    }
}
