﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
	public interface IArea
	{
		Task<List<AreaResponceModel>> GetAreaAsync(AreaRequestModel itemModel);
        //TranStatus AddArea(AreaModel itemModel);
        //TranStatus UpdateArea(AreaModel itemModel);
        //TranStatus DeleteArea(AreaModel itemModel);
    }
}
