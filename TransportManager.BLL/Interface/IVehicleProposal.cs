﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
	public interface IVehicleProposal
	{
		Task<TranStatus> AddVehicleProposal(VehicleProposalModel model);
		Task<TranStatus> UpdateVehicleProposal(int id, UpdateVehicleProposalModel model);
		Task<TranStatus> DeleteVehicleProposal(int id, int createdBy);
		Task<TranStatus> ChangeVehicleProposalStatus(int id, ChangeStatusModel model);
		Task<TranStatus> UpdateVehicleProposalStatus(UpdateVehicleProposalStatus model);
		Task<Tuple<List<GetVehicleProposalModel>, int>> GetAllVehicleProposal(PaginationModel model);
		Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByUserId(GetVehicleProposalByUserId model);
		Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalById(GetVehicleProposalById model);
		Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByOrderId(GetVehicleProposalByOrderId model);
		Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByVehicleId(GetVehicleProposalByVehicleId model);
	}
}
