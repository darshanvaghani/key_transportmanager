﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
	public interface ICity
	{
		Task<List<CityResponceModel>> GetCityAsync(CityRequestModel itemModel);
        //TranStatus AddCity(CityModel itemModel);
        //TranStatus UpdateCity(CityModel itemModel);
        //TranStatus DeleteCity(CityModel itemModel);
    }
}
