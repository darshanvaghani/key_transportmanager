﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.BLL.Interface
{
	public interface IOrder
	{
		Task<Tuple<List<GetOrderSummaryModel>, int>> GetOrderAsync(OrderRequestModel requestModel);

		Task<TranStatus> AddUpdateOrderAsync(AddOrderSummaryModel model);

		Task<TranStatus> DeleteOrderAsync(DeleteModel model);

		Task<Tuple<List<GetOrderSummaryModel>, int>> GetOrderByUserIdAsync(GetOrderByUserIdRequestModel requestModel);
		Task<Tuple<List<GetOrderSummaryModel>, int>> GetRunningOrderByUserId(GetOrderByUserIdRequestModel requestModel);

		Task<Tuple<List<GetOrderSummaryModel>, int>> GetFeedForVehicleAsync(FeedSearchModel model);

		Task<TranStatus> ChangeOrderStatus(int id, ChangeStatusModel model);

		Task<List<GetProductMaterialModel>> GetProductMaterial();

		Task<TranStatus> StatusUpdate(StatusModel model);
		Task<TranStatus> ConfirmProposalInOrder(ConfirmProposalId model);
	}
	
}
