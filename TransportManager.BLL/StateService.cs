﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.DAL;
using TransportManager.Model;

namespace TransportManager.BLL
{
    public class StateService : IState
    {
        StateRepository repository = null;

        public async Task<List<StateResponceModel>> GetState(StateRequestModel itemModel)
        {
            using (repository = new StateRepository())
            {
                return await repository.GetStateAsync(itemModel);
            }
        }


        //public IList<GetStateDropdown_Result> GetStateDropdown(out TranStatus transaction, int countryId)
        //{
        //    transaction = new TranStatus();
        //    IList<GetStateDropdown_Result> itemDetail = null;
        //    try
        //    {
        //        using (context = new StateContext())
        //        {
        //            itemDetail = context.GetStateDropdown(countryId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return itemDetail;
        //}


        //public TranStatus AddState(StateModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new StateContext())
        //        {
        //            transaction = context.AddState(itemModel.CountryId,
        //                                           itemModel.Name,
        //                                           itemModel.CreatedBy);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}

        //public TranStatus UpdateState(StateModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new StateContext())
        //        {
        //            transaction = context.UpdateState(itemModel.StateId,
        //                                              itemModel.CountryId,
        //                                              itemModel.Name,
        //                                              itemModel.IsActive,
        //                                              itemModel.CreatedBy,
        //                                              itemModel.ChangeTimeStamp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}

        //public TranStatus DeleteState(StateModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new StateContext())
        //        {
        //            transaction = context.DeleteState(itemModel.StateId,
        //                                              itemModel.IsDelete,
        //                                              itemModel.CreatedBy,
        //                                              itemModel.ChangeTimeStamp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}
    }
}
