﻿using System;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;

namespace TransportManager.BLL
{
    public class TokenService : IToken
    {
        TokenRepository tokenRepository = null;

        public bool ValidateToken(string token)
        {
            ValidateTokenRequest model = new ValidateTokenRequest();
            string decrptedToken = CommonHelper.Decrypt(token);
            model.UserId = Convert.ToInt64(decrptedToken.Substring(36));
            model.Token = decrptedToken.Substring(0, 36);

            using (tokenRepository = new TokenRepository())
            {
                return tokenRepository.ValidateToken(model).Result;
            }
        }
    }
}
