﻿using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TransportManager.BLL.Service
{
	public class VehicleProposalService : IVehicleProposal
	{
		VehicleProposalRepository vehicleProposalRepository = null;

		public async Task<TranStatus> AddVehicleProposal(VehicleProposalModel model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.AddVehicleProposal(model);
			}
		}

		public async Task<TranStatus> UpdateVehicleProposal(int id, UpdateVehicleProposalModel model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.UpdateVehicleProposal(id, model);
			}
		}

		public async Task<TranStatus> DeleteVehicleProposal(int id, int createdBy)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.DeleteVehicleProposal(id, createdBy);
			}
		}

		public async Task<TranStatus> ChangeVehicleProposalStatus(int id, ChangeStatusModel model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.ChangeVehicleProposalStatus(id, model);
			}
		}
		public async Task<TranStatus> UpdateVehicleProposalStatus(UpdateVehicleProposalStatus model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.UpdateVehicleProposalStatus(model);
			}
		}

		public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetAllVehicleProposal(PaginationModel model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.GetAllVehicleProposal(model);
			}
		}

		public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByUserId(GetVehicleProposalByUserId model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.GetVehicleProposalByUserId(model);
			}
		}

		public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalById(GetVehicleProposalById model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.GetVehicleProposalById(model);
			}
		}

		public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByOrderId(GetVehicleProposalByOrderId model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.GetVehicleProposalByOrderId(model);
			}
		}

		public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByVehicleId(GetVehicleProposalByVehicleId model)
		{
			using (vehicleProposalRepository = new VehicleProposalRepository())
			{
				return await vehicleProposalRepository.GetVehicleProposalByVehicleId(model);
			}
		}

	}
}
