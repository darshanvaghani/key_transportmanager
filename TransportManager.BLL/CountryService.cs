﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;

namespace TransportManager.BLL
{
    public class CountryService : ICountry
    {
        CountryRepository repository = null;

        public async Task<List<CountryResponceModel>> GetCountryAsync()
        {
            using (repository = new CountryRepository())
            {
                return await repository.GetCountryAsync();
            }

        }

        //public TranStatus AddCountry(CountryModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new CountryContext())
        //        {
        //            transaction = context.AddCountry(itemModel.CountryName,
        //                                             itemModel.PhoneCode,
        //                                             itemModel.CurrencySymbol,
        //                                             itemModel.TaxType,
        //                                             itemModel.DateFormat,
        //                                             itemModel.CreatedBy);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}

        //public TranStatus UpdateCountry(CountryModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new CountryContext())
        //        {
        //            transaction = context.UpdateCountry(itemModel.CountryId,
        //                                                itemModel.CountryName,
        //                                                itemModel.PhoneCode,
        //                                                itemModel.CurrencySymbol,
        //                                                itemModel.TaxType,
        //                                                itemModel.DateFormat,
        //                                                itemModel.IsActive,
        //                                                itemModel.CreatedBy,
        //                                                itemModel.ChangeTimeStamp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}

        //public TranStatus DeleteCountry(CountryModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new CountryContext())
        //        {
        //            transaction = context.DeleteCountry(itemModel.CountryId,
        //                                                itemModel.IsDelete,
        //                                                itemModel.CreatedBy,
        //                                                itemModel.ChangeTimeStamp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}
    }
}
