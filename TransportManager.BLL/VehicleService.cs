﻿using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TransportManager.BLL.Service
{
    public class VehicleService : IVehicle
    {
        VehicleRepository vehicleRepository = null;

        public async Task<TranStatus> AddVehicleType(VehicleTypeRequestModel model)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.AddVehicleType(model);
            }
        }

        public async Task<TranStatus> UpdateVehicleType(int id, UpdateVehicleTypeRequestModel model)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.UpdateVehicleType(id, model);
            }
        }

        public async Task<TranStatus> AddVehicle(VehicleModel model, IList<DocumentList> documentLists)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.AddVehicle(model, documentLists);
            }
        }

        public async Task<TranStatus> UpdateVehicle(int id, UpdateVehicleModel model, IList<DocumentList> documentLists)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.UpdateVehicle(id, model, documentLists);
            }
        }
        
        public async Task<TranStatus> DeleteVehicleType(int id, int createdBy)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.DeleteVehicleType(id, createdBy);
            }
        }
          
        public async Task<TranStatus> DeleteVehicle(int id, int createdBy)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.DeleteVehicle(id, createdBy);
            }
        }
           
        public async Task<TranStatus> ChangeVehicleStatus(int id, ChangeStatusModel model)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.ChangeVehicleStatus(id, model);
            }
        }
        
        public async Task<IList<GetVehicleTypeModel>> GetVehicleType()
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.GetVehicleType();
            }
        }
         
        public async Task<Tuple<List<GetVehicleModel>, int>> GetVehicle(GetVehicle model)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.GetVehicle(model);
            }
        }
        
        public async Task<Tuple<List<GetVehicleModel>, int>> GetVehicleByUserId(GetVehicle model)
        {
            using (vehicleRepository = new VehicleRepository())
            {
                return await vehicleRepository.GetVehicleByUserId(model);
            }
        }

    }
}
