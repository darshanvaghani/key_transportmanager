﻿using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TransportManager.BLL.Service
{
	public class UserProposalService : IUserProposal
	{
        UserProposalRepository userProposalRepository = null;

		public async Task<TranStatus> AddUserProposal(UserProposalModel model)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.AddUserProposal(model);
			}
		}

		public async Task<TranStatus> UpdateUserProposal(int id, UpdateUserProposalModel model)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.UpdateUserProposal(id, model);
			}
		}

		public async Task<TranStatus> DeleteUserProposal(int id, int createdBy)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.DeleteUserProposal(id, createdBy);
			}
		}

		public async Task<TranStatus> ChangeUserProposalStatus(ChangeUserProposalStatusModel model)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.ChangeUserProposalStatus(model);
			}
		}
		public async Task<TranStatus> UpdateUserProposalStatus(UpdateUserProposalStatus model)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.UpdateUserProposalStatus(model);
			}
		}

		public async Task<Tuple<List<GetUserProposalModel>, int>> GetAllUserProposal(PaginationModel model)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.GetAllUserProposal(model);
			}
		}

		public async Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalByUserId(GetUserProposalByUserId model)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.GetUserProposalByUserId(model);
			}
		}

		public async Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalById(GetUserProposalById model)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.GetUserProposalById(model);
			}
		}

		public async Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalByVehicleOrderId(GetUserProposalByVehicleOrderId model)
		{
			using (userProposalRepository = new UserProposalRepository())
			{
				return await userProposalRepository.GetUserProposalByVehicleOrderId(model);
			}
		}

    }
}
