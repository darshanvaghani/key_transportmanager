﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;

namespace TransportManager.BLL
{
    public class CityService : ICity
    {
        CityRepository repository = null;

        public async Task<List<CityResponceModel>> GetCityAsync(CityRequestModel itemModel)
        {
            using (repository = new CityRepository())
            {
                return await repository.GetCityAsync(itemModel);
            }
        }

        //public TranStatus AddCity(CityModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new CityContext())
        //        {
        //            transaction = context.AddCity(itemModel.StateId,
        //                                          itemModel.Name,
        //                                          itemModel.CreatedBy);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}

        //public TranStatus UpdateCity(CityModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new CityContext())
        //        {
        //            transaction = context.UpdateCity(itemModel.CityId,
        //                                              itemModel.StateId,
        //                                              itemModel.Name,
        //                                              itemModel.IsActive,
        //                                              itemModel.CreatedBy,
        //                                              itemModel.ChangeTimeStamp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}

        //public TranStatus DeleteCity(CityModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new CityContext())
        //        {
        //            transaction = context.DeleteCity(itemModel.CityId,
        //                                              itemModel.IsDelete,
        //                                              itemModel.CreatedBy,
        //                                              itemModel.ChangeTimeStamp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}
    }
}
