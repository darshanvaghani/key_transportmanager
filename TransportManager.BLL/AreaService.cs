﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;

namespace TransportManager.BLL
{
    public class AreaService : IArea
    {
        AreaRepository repository = null;

        public async Task<List<AreaResponceModel>> GetAreaAsync(AreaRequestModel itemModel)
        {
            using (repository = new AreaRepository())
            {
                return await repository.GetAreaAsync(itemModel);
            }
        }

        //public TranStatus AddArea(AreaModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new AreaContext())
        //        {
        //            transaction = context.AddArea(itemModel.StateId,
        //                                          itemModel.Name,
        //                                          itemModel.CreatedBy);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}

        //public TranStatus UpdateArea(AreaModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new AreaContext())
        //        {
        //            transaction = context.UpdateArea(itemModel.AreaId,
        //                                              itemModel.StateId,
        //                                              itemModel.Name,
        //                                              itemModel.IsActive,
        //                                              itemModel.CreatedBy,
        //                                              itemModel.ChangeTimeStamp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}

        //public TranStatus DeleteArea(AreaModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    try
        //    {
        //        using (context = new AreaContext())
        //        {
        //            transaction = context.DeleteArea(itemModel.AreaId,
        //                                              itemModel.IsDelete,
        //                                              itemModel.CreatedBy,
        //                                              itemModel.ChangeTimeStamp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction.returnMessage = ex.Message;
        //        transaction.code = Constants.Status.Error;
        //    }
        //    return transaction;
        //}
    }
}
