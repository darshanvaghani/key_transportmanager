﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.DAL
{
    public class AreaRepository : BaseRepository
    {
        public async Task<List<AreaResponceModel>> GetAreaAsync(AreaRequestModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@CityId", model.CityId);
                var city = await connection.QueryAsync<AreaResponceModel>("GetArea", parameter, commandType: CommandType.StoredProcedure);
                return city.ToList();
            }
        }

        //public TranStatus AddArea(int stateId, string name, int createdBy)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(AddArea));
        //    objDatabase.AddInParameter(_cmd, "@StateId", DbType.Int32, stateId);
        //    objDatabase.AddInParameter(_cmd, "@Name", DbType.String, name);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}

        //public TranStatus UpdateArea(int cityId,
        //                             int countryId,
        //                             string name,
        //                             bool isActive,
        //                             int createdBy,
        //                             byte[] changeTimeStamp)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(UpdateArea));
        //    objDatabase.AddInParameter(_cmd, "@AreaId", DbType.Int32, cityId);
        //    objDatabase.AddInParameter(_cmd, "@StateId", DbType.Int32, countryId);
        //    objDatabase.AddInParameter(_cmd, "@Name", DbType.String, name);
        //    objDatabase.AddInParameter(_cmd, "@IsActive", DbType.Boolean, isActive);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddInParameter(_cmd, "@ChangeTimeStamp", DbType.Binary, changeTimeStamp);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}

        //public TranStatus DeleteArea(int cityId,
        //                             bool isDelete,
        //                             int createdBy,
        //                             byte[] changeTimeStamp)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(DeleteArea));
        //    objDatabase.AddInParameter(_cmd, "@AreaId", DbType.Int32, cityId);
        //    objDatabase.AddInParameter(_cmd, "@IsDelete", DbType.Boolean, isDelete);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddInParameter(_cmd, "@ChangeTimeStamp", DbType.Binary, changeTimeStamp);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}
    }
}
