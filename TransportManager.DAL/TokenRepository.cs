﻿using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DAL
{
    public class TokenRepository : BaseRepository
    {
        public async Task<bool> ValidateToken(ValidateTokenRequest model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@Token", model.Token);
                parameter.Add("@UserId", model.UserId);
                return await connection.QueryFirstOrDefaultAsync<bool>("ValidateToken", parameter, commandType: CommandType.StoredProcedure); ;
            }
        }
    }
}
