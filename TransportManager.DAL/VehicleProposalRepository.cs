﻿using Dapper;
using TransportManager.Common;
using TransportManager.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace TransportManager.DAL
{
    public class VehicleProposalRepository : BaseRepository
    {
        public async Task<TranStatus> AddVehicleProposal(VehicleProposalModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleId", model.VehicleId);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@OrderSummaryId", model.OrderSummaryId);
                parameter.Add("@Price", model.Price);
                parameter.Add("@Description", model.Description);
                parameter.Add("@ProposalDateTime", model.ProposalDateTime);
                parameter.Add("@CreatedBy", model.CreatedBy);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(AddVehicleProposal), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> UpdateVehicleProposal(int id, UpdateVehicleProposalModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleProposalId", model.VehicleProposalId);
                parameter.Add("@VehicleId", model.VehicleId);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@OrderSummaryId", model.OrderSummaryId);
                parameter.Add("@Price", model.Price);
                parameter.Add("@Description", model.Description);
                parameter.Add("@ProposalDateTime", model.ProposalDateTime);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(UpdateVehicleProposal), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> DeleteVehicleProposal(int id, int createdBy)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleProposalId", id);
                parameter.Add("@UserId", createdBy);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(DeleteVehicleProposal), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> ChangeVehicleProposalStatus(int id, ChangeStatusModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleProposalId", id);
                parameter.Add("@IsActive", model.IsActive);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(ChangeVehicleProposalStatus), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }
        public async Task<TranStatus> UpdateVehicleProposalStatus(UpdateVehicleProposalStatus model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleProposalId", model.VehicleProposalId);
                parameter.Add("@Status", model.Status);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(UpdateVehicleProposalStatus), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetAllVehicleProposal(PaginationModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);

                var vehilceList = await connection.QueryAsync<GetVehicleProposalModel>("GetAllVehicleProposal", parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                return new Tuple<List<GetVehicleProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
            }
        }

        public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByUserId(GetVehicleProposalByUserId model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                //	var vehilceList = await connection.QueryAsync<GetVehicleProposalModel>(nameof(GetVehicleProposalByUserId), parameter, commandType: CommandType.StoredProcedure);
                var vehilceList = await connection.QueryAsync<GetVehicleProposalModel>("GetVehicleProposalByUserId", parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                return new Tuple<List<GetVehicleProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
            }
        }

        public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalById(GetVehicleProposalById model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleProposalId", model.VehicleProposalId);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                var vehilceList = await connection.QueryAsync<GetVehicleProposalModel>(nameof(GetVehicleProposalById), parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                //IEnumerable<GetVehicleProposalModel> vehilceList = await connection.QueryAsync<GetVehicleProposalModel>(nameof(GetVehicleProposalById), parameter, commandType: CommandType.StoredProcedure);
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
                return new Tuple<List<GetVehicleProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
            }
        }

        public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByOrderId(GetVehicleProposalByOrderId model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@OrderId", model.OrderId);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                IEnumerable<GetVehicleProposalModel> vehilceList = await connection.QueryAsync<GetVehicleProposalModel>(nameof(GetVehicleProposalByOrderId), parameter, commandType: CommandType.StoredProcedure);
                List<string> enumKey = new List<string>();
                foreach (var data in vehilceList)
                {
                    var enumValue = data.VehicalProof.Split(',');
                    foreach (var value in enumValue)
                    {
                        int intenumValue = Convert.ToInt32(value);
                        var enumName = Enum.GetName(typeof(Constants.DocumentType), intenumValue);
                        enumKey.Add(enumName);
                    }
                    string myCommaSeperatedString = string.Join(",", enumKey);
                    data.VehicalProof = myCommaSeperatedString;
                    data.ProfilePercentage = "85%";
                }
                int TotalDataCount = parameter.Get<int>("@RowCount");
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
                return new Tuple<List<GetVehicleProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
            }
        }

        public async Task<Tuple<List<GetVehicleProposalModel>, int>> GetVehicleProposalByVehicleId(GetVehicleProposalByVehicleId model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleId", model.VehicleId);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                IEnumerable<GetVehicleProposalModel> vehilceList = await connection.QueryAsync<GetVehicleProposalModel>(nameof(GetVehicleProposalByVehicleId), parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
                return new Tuple<List<GetVehicleProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
            }
        }
    }
}
