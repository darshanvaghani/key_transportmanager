﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.DAL
{
	public class OrderRepository : BaseRepository
	{
		public async Task<Tuple<List<GetOrderSummaryModel>, int>> GetOrderAsync(OrderRequestModel requestModel)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@OrderSummaryId", requestModel.OrderSummaryId);
				parameter.Add("@IsHuman", requestModel.IsHuman);
				parameter.Add("@PageNo", requestModel.PageNo);
				parameter.Add("@PageSize", requestModel.PageSize);
				parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
				var itemList = await connection.QueryAsync<GetOrderSummaryModel>("GetOrder", parameter, commandType: CommandType.StoredProcedure);
				int TotalDataCount = parameter.Get<int>("@RowCount");
				//    var orderDictionary = new Dictionary<long?, OrderSummaryModel>();
				//    var list = connection.Query<OrderSummaryModel, OrderDetailModel, OrderSummaryModel>("GetOrder",
				//    (order, orderDetail) =>
				//    {
				//        OrderSummaryModel orderEntry;
				//        if (!orderDictionary.TryGetValue(order.OrderSummaryId, out orderEntry))
				//        {
				//            orderEntry = order;
				//            orderEntry.ProductList = new List<OrderDetailModel>();
				//            orderDictionary.Add(orderEntry.OrderSummaryId, orderEntry);
				//        }
				//        orderEntry.ProductList.Add(orderDetail);
				//        return orderEntry;
				//    },
				//    splitOn: "OrderDetailId").Distinct().ToList();

				return new Tuple<List<GetOrderSummaryModel>, int>(itemList.ToList(), TotalDataCount);
			}
		}

		public async Task<TranStatus> AddUpdateOrderAsync(AddOrderSummaryModel model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@OrderSummaryId", model.OrderSummaryId);
				parameter.Add("@OrderNo", model.OrderNo);
				parameter.Add("@OrderTitle", model.OrderTitle);
				parameter.Add("@OrderDescription", model.OrderDescription);
				parameter.Add("@OrderDate", model.OrderDate);
				parameter.Add("@FromAddress", model.FromAddress);
				parameter.Add("@FromLandmark", model.FromLandmark);
				parameter.Add("@FromArea", model.FromArea);
				parameter.Add("@FromCityId", model.FromCityId);
				parameter.Add("@FromStateId", model.FromStateId);
				parameter.Add("@FromLAT", model.FromLAT);
				parameter.Add("@FromLONG", model.FromLONG);
				parameter.Add("@ToAddress", model.ToAddress);
				parameter.Add("@ToLandmark", model.ToLandmark);
				parameter.Add("@ToArea", model.ToArea);
				parameter.Add("@ToCityId", model.ToCityId);
				parameter.Add("@ToStateId", model.ToStateId);
				parameter.Add("@ToLAT", model.ToLAT);
				parameter.Add("@ToLONG", model.ToLONG);
				parameter.Add("@VehicleTypeId", model.VehicleTypeId);
				parameter.Add("@IsShared", model.IsShared);
				parameter.Add("@IsLoaded", model.IsLoaded);
				parameter.Add("@ContactName", model.ContactName);
				parameter.Add("@ContactNumber", model.ContactNumber);
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@Price", model.Price);
				parameter.Add("@Status", model.Status);
				DataTable dataTable = CommonHelper.ToDataTable(model.ProductList);
				parameter.Add("@ProductList", dataTable.AsTableValuedParameter("dbo.OrderProductList"));
				parameter.Add("@CreatedById", model.CreatedById);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);

				await connection.QueryAsync("AddUpdateOrder", parameter, commandType: CommandType.StoredProcedure);

				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");

				return transaction;
			}
		}

		public async Task<TranStatus> DeleteOrderAsync(DeleteModel model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@OrderSummaryId", model.Id);
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);

				await connection.QueryAsync("DeleteOrder", parameter, commandType: CommandType.StoredProcedure);

				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");

				return transaction;
			}
		}

		public async Task<Tuple<List<GetOrderSummaryModel>, int>> GetOrderByUserIdAsync(GetOrderByUserIdRequestModel requestModel)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@UserId", requestModel.UserId);
				parameter.Add("@IsHuman", requestModel.IsHuman);
				parameter.Add("@PageNo", requestModel.PageNo);
				parameter.Add("@PageSize", requestModel.PageSize);
				parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
				var itemList = await connection.QueryAsync<GetOrderSummaryModel>("GetOrderByUserId", parameter, commandType: CommandType.StoredProcedure);
				int TotalDataCount = parameter.Get<int>("@RowCount");
				return new Tuple<List<GetOrderSummaryModel>, int>(itemList.ToList(), TotalDataCount);
			}
		}


		public async Task<Tuple<List<GetOrderSummaryModel>, int>> GetRunningOrderByUserId(GetOrderByUserIdRequestModel requestModel)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@UserId", requestModel.UserId);
				parameter.Add("@IsHuman", requestModel.IsHuman);
				parameter.Add("@PageNo", requestModel.PageNo);
				parameter.Add("@PageSize", requestModel.PageSize);
				parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
				var itemList = await connection.QueryAsync<GetOrderSummaryModel>("GetRunningOrderByUserId", parameter, commandType: CommandType.StoredProcedure);
				int TotalDataCount = parameter.Get<int>("@RowCount");

				return new Tuple<List<GetOrderSummaryModel>, int>(itemList.ToList(), TotalDataCount);
			}
		}


		public async Task<Tuple<List<GetOrderSummaryModel>, int>> GetFeedForVehicleAsync(FeedSearchModel model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@City", model.City);
				parameter.Add("@SearchText", model.SearchText);
				parameter.Add("@State", model.State);
				parameter.Add("@Area", model.Area);
				parameter.Add("@FromPrice", model.FromPrice);
				parameter.Add("@ToPrice", model.ToPrice);
				parameter.Add("@IsShared", model.IsLoaded);
				parameter.Add("@IsLoaded", model.IsShared);
				parameter.Add("@IsHuman", model.IsHuman);
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@PageNo", model.PageNo);
				parameter.Add("@PageSize", model.PageSize);
				parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
				var itemList = await connection.QueryAsync<GetOrderSummaryModel>("GetFeedForVehicle", parameter, commandType: CommandType.StoredProcedure);
				int TotalDataCount = parameter.Get<int>("@RowCount");

				return new Tuple<List<GetOrderSummaryModel>, int>(itemList.ToList(), TotalDataCount);
			}
		}

		public async Task<TranStatus> ChangeOrderStatus(int id, ChangeStatusModel model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@OrderSummaryId", id);
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@IsActive", model.IsActive);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync("ChangeOrderStatus", parameter, commandType: CommandType.StoredProcedure);

				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");

				return transaction;
			}
		}

		public async Task<List<GetProductMaterialModel>> GetProductMaterial()
		{
			List<GetProductMaterialModel> lst = new List<GetProductMaterialModel>();
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();
				DynamicParameters parameter = new DynamicParameters();
				var vehilceTypeList = await connection.QueryAsync<GetProductMaterialModel>(nameof(GetProductMaterial), commandType: CommandType.StoredProcedure);
				lst = vehilceTypeList?.ToList() ?? new List<GetProductMaterialModel>();
				return lst;
			}
		}

		public async Task<TranStatus> StatusUpdate(StatusModel model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();
				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@OrderSummaryId", model.OrderSummaryId);
				parameter.Add("@Status", model.OrderStatus);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync("UpdateStatus", parameter, commandType: CommandType.StoredProcedure);

				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}
		public async Task<TranStatus> ConfirmProposalInOrder(ConfirmProposalId model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();
				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@OrderId", model.OrderId);
				parameter.Add("@ProposalId", model.ProposalId);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync("ConfirmProposalInOrder", parameter, commandType: CommandType.StoredProcedure);

				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}
	}
}
