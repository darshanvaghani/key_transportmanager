﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.DAL
{
    public class VehicleOrderRepository : BaseRepository
    {
        public async Task<TranStatus> AddUpdateVehicleOrder(VehicleOrderSummaryModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleOrderSummaryId", model.VehicleOrderSummaryId);
                parameter.Add("@VehicleOrderNo", model.VehicleOrderNo);
                parameter.Add("@VehicleOrderTitle", model.VehicleOrderTitle);
                parameter.Add("@VehicleOrderDescription", model.VehicleOrderDescription);
                parameter.Add("@VehicleOrderDate", model.VehicleOrderDate);
                parameter.Add("@FromAddress", model.FromAddress);
                parameter.Add("@FromLandmark", model.FromLandmark);
                parameter.Add("@FromArea", model.FromArea);
                parameter.Add("@FromCityId", model.FromCityId);
                parameter.Add("@FromStateId", model.FromStateId);
                parameter.Add("@FromLAT", model.FromLAT);
                parameter.Add("@FromLONG", model.FromLONG);
                parameter.Add("@ToAddress", model.ToAddress);
                parameter.Add("@ToLandmark", model.ToLandmark);
                parameter.Add("@ToArea", model.ToArea);
                parameter.Add("@ToCityId", model.ToCityId);
                parameter.Add("@ToStateId", model.ToStateId);
                parameter.Add("@ToLAT", model.ToLAT);
                parameter.Add("@ToLONG", model.ToLONG);
                parameter.Add("@VehicleId", model.VehicleId);
                parameter.Add("@IsShared", model.IsShared);
                parameter.Add("@IsLoaded", model.IsLoaded);
                parameter.Add("@ContactName", model.ContactName);
                parameter.Add("@ContactNumber", model.ContactNumber);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@Price", model.Price);
                parameter.Add("@Size", model.Size);
                parameter.Add("@Qty", model.Qty);
                parameter.Add("@UoM", model.UoM);
                parameter.Add("@Status", model.Status);
                parameter.Add("@ProductState", model.ProductState);
                parameter.Add("@CreatedById", model.CreatedById);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);

                await connection.QueryAsync(nameof(AddUpdateVehicleOrder), parameter, commandType: CommandType.StoredProcedure);

                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");

                return transaction;
            }
        }

        public async Task<TranStatus> DeleteVehicleOrder(DeleteModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleOrderSummaryId", model.Id);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);

                await connection.QueryAsync(nameof(DeleteVehicleOrder), parameter, commandType: CommandType.StoredProcedure);

                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");

                return transaction;
            }
        }

        public async Task<TranStatus> ChangeVehicleOrderStatus(int id, ChangeStatusModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleOrderSummaryId", id);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);

                await connection.QueryAsync(nameof(ChangeVehicleOrderStatus), parameter, commandType: CommandType.StoredProcedure);

                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");

                return transaction;
            }
        }

        public async Task<Tuple<List<VehicleOrderSummaryDetails>, int>> GetVehicleOrder(GetVehicleOrder model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@IsHuman", model.IsHuman);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@SearchText", model.SearchText);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                var itemList = await connection.QueryAsync<VehicleOrderSummaryDetails>("GetVehicleOrder", parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                return new Tuple<List<VehicleOrderSummaryDetails>, int>(itemList.ToList(), TotalDataCount);
            }
        }

        public async Task<List<VehicleOrderSummaryDetails>> GetVehicleOrderById(GetVehicleOrder model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleOrderSummaryId", model.Id);
                parameter.Add("@IsHuman", model.IsHuman);
                var itemList = await connection.QueryAsync<VehicleOrderSummaryDetails>(nameof(GetVehicleOrderById), parameter, commandType: CommandType.StoredProcedure);
                return itemList.ToList();
            }
        }

        public async Task<Tuple<List<VehicleOrderSummaryDetails>, int>> GetVehicleOrderByUserId(GetVehicleOrder model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", model.Id);
                parameter.Add("@IsHuman", model.IsHuman);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                var itemList = await connection.QueryAsync<VehicleOrderSummaryDetails>("GetVehicleOrderByUserId", parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                return new Tuple<List<VehicleOrderSummaryDetails>, int>(itemList.ToList(), TotalDataCount);
                //return itemList.ToList();
            }
        }

        public async Task<TranStatus> VehicleOrderStatusUpdate(VehicleOrderStatusUpdateModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@OrderSummaryId", model.VehiclrOrderId);
                parameter.Add("@Status", model.VehiclrOrderStatus);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync("UpdateStatus", parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }
        public async Task<TranStatus> ConfirmProposalInVehicleOrder(ConfirmProposalIdInVehicleOrder model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleOrderId", model.VehicleOrderId);
                parameter.Add("@ProposalId", model.ProposalId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync("ConfirmProposalInVehicleOrder", parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }
    }
}
