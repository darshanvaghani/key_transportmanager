﻿using Dapper;
using TransportManager.Common;
using TransportManager.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace TransportManager.DAL
{
    public class UserProposalRepository : BaseRepository
    {
        public async Task<TranStatus> AddUserProposal(UserProposalModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@VehicleOrderSummaryId", model.VehicleOrderSummaryId);
                parameter.Add("@Price", model.Price);
                parameter.Add("@Size", model.Size);
                parameter.Add("@UOM", model.UOM);
                parameter.Add("@QTY", model.QTY);
                parameter.Add("@Description", model.Description);
                parameter.Add("@ProposalDateTime", model.ProposalDateTime);
                parameter.Add("@CreatedBy", model.CreatedBy);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(AddUserProposal), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> UpdateUserProposal(int id, UpdateUserProposalModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserProposalId", model.UserProposalId);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@VehicleOrderSummaryId", model.VehicleOrderSummaryId);
                parameter.Add("@Price", model.Price);
                parameter.Add("@Size", model.Size);
                parameter.Add("@UOM", model.UOM);
                parameter.Add("@QTY", model.QTY);
                parameter.Add("@Description", model.Description);
                parameter.Add("@ProposalDateTime", model.ProposalDateTime);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(UpdateUserProposal), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> DeleteUserProposal(int id, int createdBy)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserProposalId", id);
                parameter.Add("@UserId", createdBy);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(DeleteUserProposal), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> ChangeUserProposalStatus(ChangeUserProposalStatusModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserProposalId", model.UserProposalId);
                parameter.Add("@IsActive", model.IsActive);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(ChangeUserProposalStatus), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }
        public async Task<TranStatus> UpdateUserProposalStatus(UpdateUserProposalStatus model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserProposalId", model.UserProposalId);
                parameter.Add("@Status", model.Status);
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync(nameof(UpdateUserProposalStatus), parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }
        public async Task<Tuple<List<GetUserProposalModel>, int>> GetAllUserProposal(PaginationModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);

                var vehilceList = await connection.QueryAsync<GetUserProposalModel>("GetAllUserProposal", parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                return new Tuple<List<GetUserProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
            }
        }

        public async Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalByUserId(GetUserProposalByUserId model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", model.UserId);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                //	var vehilceList = await connection.QueryAsync<GetVehicleProposalModel>(nameof(GetVehicleProposalByUserId), parameter, commandType: CommandType.StoredProcedure);
                var vehilceList = await connection.QueryAsync<GetUserProposalModel>("GetUserProposalByUserId", parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                return new Tuple<List<GetUserProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
            }
        }

        public async Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalById(GetUserProposalById model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserProposalId", model.UserProposalId);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                var vehilceList = await connection.QueryAsync<GetUserProposalModel>("GetUserProposalById", parameter, commandType: CommandType.StoredProcedure);
                //var vehilceList = await connection.QueryAsync<GetUserProposalModel>(nameof(GetUserProposalById), parameter, commandType: CommandType.StoredProcedure);
                int TotalDataCount = parameter.Get<int>("@RowCount");
                //IEnumerable<GetVehicleProposalModel> vehilceList = await connection.QueryAsync<GetVehicleProposalModel>(nameof(GetVehicleProposalById), parameter, commandType: CommandType.StoredProcedure);
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
                return new Tuple<List<GetUserProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
            }
        }

        public async Task<Tuple<List<GetUserProposalModel>, int>> GetUserProposalByVehicleOrderId(GetUserProposalByVehicleOrderId model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@VehicleOrderSummaryId", model.VehicleOrderSummaryId);
                parameter.Add("@PageNo", model.PageNo);
                parameter.Add("@PageSize", model.PageSize);
                parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
                var vehilceList = await connection.QueryAsync<GetUserProposalModel>("GetUserProposalByVehicleOrderId", parameter, commandType: CommandType.StoredProcedure);
                //IEnumerable<GetUserProposalModel> vehilceList = await connection.QueryAsync<GetUserProposalModel>(nameof(GetUserProposalByOrderId), parameter, commandType: CommandType.StoredProcedure);
                List<string> enumKey = new List<string>();
                int TotalDataCount = parameter.Get<int>("@RowCount");
                //return vehilceList?.ToList() ?? new List<GetVehicleProposalModel>();
                return new Tuple<List<GetUserProposalModel>, int>(vehilceList.ToList(), TotalDataCount);
            }
        }
    }
}
