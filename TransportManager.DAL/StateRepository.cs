﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DAL
{
    public class StateRepository : BaseRepository
    {
        public async Task<List<StateResponceModel>> GetStateAsync(StateRequestModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@CountryId", model.CountryId);
                var state = await connection.QueryAsync<StateResponceModel>("GetState", parameter, commandType: CommandType.StoredProcedure);
                return state.ToList();
            }
        }

        //public TranStatus AddState(int countryId, 
        //                           string name, 
        //                           int createdBy)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(AddState));
        //    objDatabase.AddInParameter(_cmd, "@CountryId", DbType.Int32, countryId);
        //    objDatabase.AddInParameter(_cmd, "@Name", DbType.String, name);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}

        //public TranStatus UpdateState(int stateId, 
        //                              int countryId, 
        //                              string name,
        //                              bool isActive, 
        //                              int createdBy, 
        //                              byte[] changeTimeStamp)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(UpdateState));
        //    objDatabase.AddInParameter(_cmd, "@StateId", DbType.Int32, stateId);
        //    objDatabase.AddInParameter(_cmd, "@CountryId", DbType.Int32, countryId);
        //    objDatabase.AddInParameter(_cmd, "@Name", DbType.String, name);
        //    objDatabase.AddInParameter(_cmd, "@IsActive", DbType.Boolean, isActive);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddInParameter(_cmd, "@ChangeTimeStamp", DbType.Binary, changeTimeStamp);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}

        //public TranStatus DeleteState(int stateId, 
        //                              bool isDelete, 
        //                              int createdBy, 
        //                              byte[] changeTimeStamp)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(DeleteState));
        //    objDatabase.AddInParameter(_cmd, "@StateId", DbType.Int32, stateId);
        //    objDatabase.AddInParameter(_cmd, "@IsDelete", DbType.Boolean, isDelete);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddInParameter(_cmd, "@ChangeTimeStamp", DbType.Binary, changeTimeStamp);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}
    }
}
