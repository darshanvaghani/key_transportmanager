﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TransportManager.Model;

namespace TransportManager.DAL
{
    public class CountryRepository : BaseRepository
    {
        public async Task<List<CountryResponceModel>> GetCountryAsync()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                var country = await connection.QueryAsync<CountryResponceModel>("GetCountry", commandType: CommandType.StoredProcedure);
                return country.ToList();
            }
        }

        //public TranStatus AddCountry(string countryName,
        //                             string phoneCode,
        //                             string currencySymbol,
        //                             string taxType,
        //                             string dateFormat,
        //                             int createdBy)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(AddCountry));
        //    objDatabase.AddInParameter(_cmd, "@CountryName", DbType.String, countryName);
        //    objDatabase.AddInParameter(_cmd, "@PhoneCode", DbType.String, phoneCode);
        //    objDatabase.AddInParameter(_cmd, "@CurrencySymbol", DbType.String, currencySymbol);
        //    objDatabase.AddInParameter(_cmd, "@TaxType", DbType.String, taxType);
        //    objDatabase.AddInParameter(_cmd, "@DateFormat", DbType.String, dateFormat);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}

        //public TranStatus UpdateCountry(int countryId,
        //                                string countryName,
        //                                string phoneCode,
        //                                string currencySymbol,
        //                                string taxType,
        //                                string dateFormat,
        //                                bool isActive,
        //                                int createdBy,
        //                                byte[] changeTimeStamp)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(UpdateCountry));
        //    objDatabase.AddInParameter(_cmd, "@CountryId", DbType.Int32, countryId);
        //    objDatabase.AddInParameter(_cmd, "@CountryName", DbType.String, countryName);
        //    objDatabase.AddInParameter(_cmd, "@PhoneCode", DbType.String, phoneCode);
        //    objDatabase.AddInParameter(_cmd, "@CurrencySymbol", DbType.String, currencySymbol);
        //    objDatabase.AddInParameter(_cmd, "@TaxType", DbType.String, taxType);
        //    objDatabase.AddInParameter(_cmd, "@DateFormat", DbType.String, dateFormat);
        //    objDatabase.AddInParameter(_cmd, "@IsActive", DbType.Boolean, isActive);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddInParameter(_cmd, "@ChangeTimeStamp", DbType.Binary, changeTimeStamp);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}

        //public TranStatus DeleteCountry(int countryId,
        //                                bool isDelete,
        //                                int createdBy,
        //                                byte[] changeTimeStamp)
        //{
        //    TranStatus transaction = new TranStatus();
        //    SqlDatabase objDatabase = new SqlDatabase(this.ConnectionString);

        //    DbCommand _cmd = objDatabase.GetStoredProcCommand(nameof(DeleteCountry));
        //    objDatabase.AddInParameter(_cmd, "@CountryId", DbType.Int32, countryId);
        //    objDatabase.AddInParameter(_cmd, "@IsDelete", DbType.Boolean, isDelete);
        //    objDatabase.AddInParameter(_cmd, "@CreatedBy", DbType.Int32, createdBy);
        //    objDatabase.AddInParameter(_cmd, "@ChangeTimeStamp", DbType.Binary, changeTimeStamp);
        //    objDatabase.AddOutParameter(_cmd, "@Message", DbType.String, 500);
        //    objDatabase.AddOutParameter(_cmd, "@Code", DbType.Int32, 10);
        //    objDatabase.ExecuteNonQuery(_cmd);

        //    transaction.returnMessage = objDatabase.GetParameterValue(_cmd, "@Message").ToString();
        //    transaction.code = Convert.ToInt32(objDatabase.GetParameterValue(_cmd, "@Code"));
        //    return transaction;
        //}
    }
}
