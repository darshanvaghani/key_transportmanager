﻿using Dapper;
using TransportManager.Common;
using TransportManager.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace TransportManager.DAL
{
    public class UserRepository : BaseRepository
    {
        public async Task<Tuple<LoginResponseModel, TranStatus>> CheckLoginUser(LoginRequestModel model)
        {
            LoginResponseModel user = new LoginResponseModel();

            TranStatus transaction = new TranStatus();
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@Username", model.UserName);
                parameter.Add("@Password", model.Password);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);

                user = await connection.QueryFirstOrDefaultAsync<LoginResponseModel>("CheckLoginUser", parameter, commandType: CommandType.StoredProcedure);
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
            }
            return new Tuple<LoginResponseModel, TranStatus>(user, transaction);
        }

        public async Task<Tuple<LoginResponseModel, TranStatus>> Register(RegisterModel model)
        {
            LoginResponseModel user = new LoginResponseModel();
            TranStatus transaction = new TranStatus();
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@Name", model.Name);
                parameter.Add("@EmailId", model.EmailId);
                parameter.Add("@Password", model.Password);
                parameter.Add("@Phone", model.Phone);
                parameter.Add("@Address1", model.Address1);
                parameter.Add("@Address2", model.Address2);
                parameter.Add("@Image", model.Image);
                parameter.Add("@BirthDate", model.BirthDate, DbType.Date);
                parameter.Add("@Language", model.Language);
                parameter.Add("@CountryId", model.CountryId);
                parameter.Add("@StateId", model.StateId);
                parameter.Add("@CityId", model.CityId);
                parameter.Add("@PinCode", model.PinCode);
                parameter.Add("@Latitude", model.Latitude);
                parameter.Add("@Longitude", model.Longitude);
                parameter.Add("@RoleId", model.RoleId);
                parameter.Add("@DeviceInfo", model.DeviceInfo);
                parameter.Add("@GoogleLoginId", model.GoogleLoginId);
                parameter.Add("@FacebookLoginId", model.FacebookLoginId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                user = await connection.QueryFirstOrDefaultAsync<LoginResponseModel>("AddUser", parameter, commandType: CommandType.StoredProcedure);
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
            }
            return new Tuple<LoginResponseModel, TranStatus>(user, transaction);
        }

        public async Task<TranStatus> UpdateUser(UpdateUserModel model, long userId)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@Name", model.Name);
                parameter.Add("@UserId", userId);
                parameter.Add("@Phone", model.Phone);
                parameter.Add("@Address1", model.Address1);
                parameter.Add("@Address2", model.Address2);
                parameter.Add("@CountryId", model.CountryId);
                parameter.Add("@StateId", model.StateId);
                parameter.Add("@CityId", model.CityId);
                parameter.Add("@PinCode", model.PinCode);
                parameter.Add("@Latitude", model.Latitude);
                parameter.Add("@Longitude", model.Longitude);
                parameter.Add("@DeviceInfo", model.DeviceInfo);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync("UpdateUser", parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> Logout(long userId)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", userId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryAsync("Logout", parameter, commandType: CommandType.StoredProcedure);
                TranStatus transaction = new TranStatus();
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> ChangePassword(long userId, ChangePasswordRequestModel changePassword)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                TranStatus transaction = new TranStatus();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", userId);
                parameter.Add("@OldPassword", changePassword.OldPassword);
                parameter.Add("@NewPassword", changePassword.NewPassword);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);

                await connection.QueryAsync("ChangePassword", parameter, commandType: CommandType.StoredProcedure);

                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<TranStatus> ChangeStatus(long userId, ChangeStatusModel model)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();
                TranStatus transaction = new TranStatus();
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", userId);
                parameter.Add("@IsActive", model.IsActive);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);

                await connection.QueryAsync("ChangeStatus", parameter, commandType: CommandType.StoredProcedure);

                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<List<LoginHistoryResponseModel>> GetLoginHistory(long id)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserID", id);
                var result = await connection.QueryAsync<LoginHistoryResponseModel>("GetLoginHistory", parameter, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<TranStatus> Delete(long userId)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                TranStatus transaction = new TranStatus();
                await connection.OpenAsync();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", userId);
                parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);
                parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
                await connection.QueryMultipleAsync("DeleteUserbyId", parameter, commandType: CommandType.StoredProcedure);
                transaction.returnMessage = parameter.Get<string>("@Message");
                transaction.code = parameter.Get<int>("@Code");
                return transaction;
            }
        }

        public async Task<List<UserResponseModel>> GetUser(long id)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@UserId", id);
                var result = await connection.QueryAsync<UserResponseModel>("GetUser", parameter, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public async Task<IList<UserRole>> GetRole()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<UserRole>("GetRole", commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }
    }
}
