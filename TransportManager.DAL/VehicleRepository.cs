﻿using Dapper;
using TransportManager.Common;
using TransportManager.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace TransportManager.DAL
{
	public class VehicleRepository : BaseRepository
	{
		public async Task<TranStatus> AddVehicleType(VehicleTypeRequestModel model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@VehicleType", model.VehicleType);
				parameter.Add("@Image", model.Image);
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync(nameof(AddVehicleType), parameter, commandType: CommandType.StoredProcedure);
				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}

		public async Task<TranStatus> UpdateVehicleType(int id, UpdateVehicleTypeRequestModel model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@VehicleTypeId", id);
				parameter.Add("@VehicleType", model.VehicleType);
				parameter.Add("@Image", model.Image);
				parameter.Add("@IsActive", model.IsActive);
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync(nameof(UpdateVehicleType), parameter, commandType: CommandType.StoredProcedure);
				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}

		public async Task<TranStatus> AddVehicle(VehicleModel model, IList<DocumentList> documentLists)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@VehicleTypeId", model.VehicleTypeId);
				parameter.Add("@VehicleNumber", model.VehicleNumber);
				parameter.Add("@SerialNumber", model.SerialNumber);
				parameter.Add("@MaxCapacity", model.MaxCapacity);
				DataTable dataTable = CommonHelper.ToDataTable(documentLists.ToList());
				parameter.Add("@DocumentList", dataTable.AsTableValuedParameter("dbo.DocumentList"));
				parameter.Add("@MaxCapacity", model.MaxCapacity);
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@CreatedBy", model.CreatedBy);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync(nameof(AddVehicle), parameter, commandType: CommandType.StoredProcedure);
				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}

		public async Task<TranStatus> UpdateVehicle(int id, UpdateVehicleModel model, IList<DocumentList> documentLists)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@VehicleId", id);
				parameter.Add("@VehicleTypeId", model.VehicleTypeId);
				parameter.Add("@VehicleNumber", model.VehicleNumber);
				parameter.Add("@SerialNumber", model.SerialNumber);
				parameter.Add("@MaxCapacity", model.MaxCapacity);
				DataTable dataTable = CommonHelper.ToDataTable(documentLists.ToList());
				parameter.Add("@DocumentList", dataTable.AsTableValuedParameter("dbo.DocumentList"));
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@CreatedBy", model.CreatedBy);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync(nameof(UpdateVehicle), parameter, commandType: CommandType.StoredProcedure);
				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}

		public async Task<TranStatus> DeleteVehicleType(int id, int createdBy)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@VehicleTypeId", id);
				parameter.Add("@UserId", createdBy);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync(nameof(DeleteVehicleType), parameter, commandType: CommandType.StoredProcedure);
				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}

		public async Task<TranStatus> DeleteVehicle(int id, int createdBy)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@VehicleId", id);
				parameter.Add("@UserId", createdBy);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync(nameof(DeleteVehicle), parameter, commandType: CommandType.StoredProcedure);
				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}

		public async Task<TranStatus> ChangeVehicleStatus(int id, ChangeStatusModel model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@VehicleId", id);
				parameter.Add("@IsActive", model.IsActive);
				parameter.Add("@UserId", model.UserId);
				parameter.Add("@Message", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
				parameter.Add("@Code", dbType: DbType.Int32, direction: ParameterDirection.Output);
				await connection.QueryAsync(nameof(DeleteVehicle), parameter, commandType: CommandType.StoredProcedure);
				TranStatus transaction = new TranStatus();
				transaction.returnMessage = parameter.Get<string>("@Message");
				transaction.code = parameter.Get<int>("@Code");
				return transaction;
			}
		}

		public async Task<IList<GetVehicleTypeModel>> GetVehicleType()
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				IEnumerable<GetVehicleTypeModel> vehilceTypeList = await connection.QueryAsync<GetVehicleTypeModel>(nameof(GetVehicleType), commandType: CommandType.StoredProcedure);
				return vehilceTypeList?.ToList() ?? new List<GetVehicleTypeModel>();
			}
		}

		public async Task<Tuple<List<GetVehicleModel>, int>> GetVehicle(GetVehicle model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@VehicleId", model.Id);
				parameter.Add("@PageNo", model.PageNo);
				parameter.Add("@PageSize", model.PageSize);
				parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
				//var vehilceList = await connection.QueryAsync<GetVehicleModel>(nameof(GetVehicle), parameter, commandType: CommandType.StoredProcedure);
				var itemList = await connection.QueryAsync<GetVehicleModel>("GetVehicle", parameter, commandType: CommandType.StoredProcedure);
				int TotalDataCount = parameter.Get<int>("@RowCount");
				return new Tuple<List<GetVehicleModel>, int>(itemList.ToList(), TotalDataCount);
				
				//return vehilceList?.ToList() ?? new List<GetVehicleModel>();

			}
		}

		public async Task<Tuple<List<GetVehicleModel>, int>> GetVehicleByUserId(GetVehicle model)
		{
			using (var connection = new SqlConnection(ConnectionString))
			{
				await connection.OpenAsync();

				DynamicParameters parameter = new DynamicParameters();
				parameter.Add("@UserId", model.Id);
				parameter.Add("@PageNo", model.PageNo);
				parameter.Add("@PageSize", model.PageSize);
				parameter.Add("@RowCount", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 10);
				var itemList = await connection.QueryAsync<GetVehicleModel>("GetVehicleByUserId", parameter, commandType: CommandType.StoredProcedure);
				//IEnumerable<GetVehicleModel> vehilceList = await connection.QueryAsync<GetVehicleModel>(nameof(GetVehicleByUserId), parameter, commandType: CommandType.StoredProcedure);
				int TotalDataCount = parameter.Get<int>("@RowCount");
				return new Tuple<List<GetVehicleModel>, int>(itemList.ToList(), TotalDataCount);
				// return vehilceList?.ToList() ?? new List<GetVehicleModel>();
			}
		}

	}
}
