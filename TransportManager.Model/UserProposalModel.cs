﻿
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TransportManager.Model
{
    public class UserProposalModel
    {
        public int UserId { get; set; }
        public int VehicleOrderSummaryId { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public string UOM { get; set; }
        public int QTY { get; set; }
        public string Description { get; set; }
        public DateTime? ProposalDateTime { get; set; }
        public int CreatedBy { get; set; }
    }

    public class UpdateUserProposalModel
    {
        public int UserProposalId { get; set; }
        public int UserId { get; set; }
        public int VehicleOrderSummaryId { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public string UOM { get; set; }
        public int QTY { get; set; }
        public string Description { get; set; }
        public DateTime? ProposalDateTime { get; set; }
        public int CreatedBy { get; set; }
    }

    public class GetUserProposalModel
    {
        //Vehicle Details
        public int UserProposalId { get; set; }
        public int UserId { get; set; }
        public int VehicleOrderSummaryId { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public string UOM { get; set; }
        public int QTY { get; set; }
        public string Description { get; set; }
        public DateTime? ProposalDateTime { get; set; }
        public bool Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        //User Details
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string DeviceInfo { get; set; }
        public DateTime? BirthDate { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public int PinCode { get; set; }
    }

    public class GetUserProposalByUserId : PaginationModel
    {
        public long UserId { get; set; }
    }
    public class UpdateUserProposalStatus
    {
        public long UserProposalId { get; set; }
        public long Status { get; set; }
        public long UserId { get; set; }
    }

    public class GetUserProposalById : PaginationModel
    {
        public int UserProposalId { get; set; }
    }
    public class GetUserProposalByVehicleOrderId : PaginationModel
    {
        public int VehicleOrderSummaryId { get; set; }
    }
    public class GetUserProposal : PaginationModel
    {
        public int Id { get; set; }
    }

    public class ChangeUserProposalStatusModel
    {
        public bool IsActive { get; set; }
        public int UserProposalId { get; set; }
        public int UserId { get; set; }
    }

}
