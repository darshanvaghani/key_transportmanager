﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static TransportManager.Common.Constants;

namespace TransportManager.Model
{
    public class RegisterModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string EmailId { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Image { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Language { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public string PinCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int RoleId { get; set; }
        public string DeviceInfo { get; set; }
        public string GoogleLoginId { get; set; }
        public string FacebookLoginId { get; set; }
    }
    public class UpdateUserModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string PinCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string DeviceInfo { get; set; }
    }

    public class ChangePasswordRequestModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class ChangeStatusModel
    {
        public bool IsActive { get; set; }
        public int UserId { get; set; }
    }

    public class LoginHistoryResponseModel
    {
        public string Info { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class UserDeviceDetail
    {
        public string Browser { get; set; }
        public string BrowserVersion { get; set; }
        public string OS { get; set; }
        public string OSVesrion { get; set; }
        public string Device { get; set; }
        public string DeviceBrand { get; set; }
        public bool DeviceIsSpider { get; set; }
        public string DeviceModel { get; set; }
        public string CreatedIpv4 { get; set; }
        public string CreatedIpv6 { get; set; }
    }

    public class LoginRequestModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class LoginResponseModel
    {
        public long UserId { get; set; }
        public string EmailId { get; set; }
        public string Username { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Image { get; set; }
        public DateTime BirthDate { get; set; }
        public string Language { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int StateId { get; set; }
        public string StateIdName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string PinCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string RoleListJson { get; set; }
        [JsonProperty(PropertyName = "AuthToken")]
        public string Token { get; set; }
    }
    public class UserRole
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
    public class UserResponseModel
    {
        public long UserId { get; set; }
        public string EmailId { get; set; }
        public string Username { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int StateId { get; set; }
        public string StateIdName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string PinCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        [JsonIgnore]
        public string RoleListJson { get; set; }
        public IList<UserRole> RoleList
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<IList<UserRole>>(RoleListJson);
                }
                catch (Exception e)
                {
                    return new List<UserRole>();
                }
            }
        }
    }
}
