﻿using Newtonsoft.Json;
using System;

namespace TransportManager.Model
{
    public class StateRequestModel
    {
        public int CountryId { get; set; }
        public string SearchText { get; set; }
    }

    public partial class StateResponceModel
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
    }

    public partial class StateModel
    {
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public byte[] ChangeTimeStamp { get; set; }
    }
}
