﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static TransportManager.Common.Constants;

namespace TransportManager.Model
{
    public class VehicleOrderSummaryDetails
    {
        public long VehicleOrderSummaryId { get; set; }
        public string VehicleOrderNo { get; set; }
        public string VehicleOrderTitle { get; set; }
        public string VehicleOrderDescription { get; set; }
        public DateTime VehicleOrderDate { get; set; }
        [JsonIgnore]
        public string FormatedOrderDate
        {
            get
            {
                return VehicleOrderDate.ToString("dd-MM-yyyy");
            }
        }
        public string FromAddress { get; set; }
        public string FromLandmark { get; set; }
        public string FromArea { get; set; }
        public int FromCityId { get; set; }
        public int FromStateId { get; set; }
        public string FromLAT { get; set; }
        public string FromLONG { get; set; }
        public string ToAddress { get; set; }
        public string ToLandmark { get; set; }
        public string ToArea { get; set; }
        public int ToCityId { get; set; }
        public int ToStateId { get; set; }
        public string ToLAT { get; set; }
        public string ToLONG { get; set; }
        public int VehicleId { get; set; }
        public bool IsShared { get; set; }
        public bool IsLoaded { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public int UserId { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public int Qty { get; set; }
        public string UoM { get; set; }
        public int Status { get; set; }
        public int ProductState { get; set; }
        public string StatusName
        {
            get
            {
                return ((OrderStatus)Status).ToString();
            }
        }
        public int CreatedById { get; set; }

        //Vehicle Details
        public string VehicleImage { get; set; }
        public int VehicleTypeId { get; set; }
        public string VehicleNumber { get; set; }
        public string VehicleType { get; set; }
        public int DocumentId { get; set; }
        public string VehicleTypeImage { get; set; }
    }
    public class GetVehicleOrder : PaginationModel
    {
        public int Id { get; set; }
        public bool IsHuman { get; set; }
        public string SearchText { get; set; }
    }

    public class VehicleOrderSummaryModel
    {
        public long VehicleOrderSummaryId { get; set; }
        public string VehicleOrderNo { get; set; }
        public string VehicleOrderTitle { get; set; }
        public string VehicleOrderDescription { get; set; }
        public DateTime VehicleOrderDate { get; set; }
        [JsonIgnore]
        public string FormatedOrderDate
        {
            get
            {
                return VehicleOrderDate.ToString("dd-MM-yyyy");
            }
        }
        public string FromAddress { get; set; }
        public string FromLandmark { get; set; }
        public string FromArea { get; set; }
        public int FromCityId { get; set; }
        public int FromStateId { get; set; }
        public string FromLAT { get; set; }
        public string FromLONG { get; set; }
        public string ToAddress { get; set; }
        public string ToLandmark { get; set; }
        public string ToArea { get; set; }
        public int ToCityId { get; set; }
        public int ToStateId { get; set; }
        public string ToLAT { get; set; }
        public string ToLONG { get; set; }
        public int VehicleId { get; set; }
        public bool IsShared { get; set; }
        public bool IsLoaded { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public int UserId { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public int Qty { get; set; }
        public string UoM { get; set; }
        public int Status { get; set; }
        public int ProductState { get; set; }
        public string StatusName
        {
            get
            {
                return ((OrderStatus)Status).ToString();
            }
        }
        public int CreatedById { get; set; }
    }
    public class VehicleOrderStatusUpdateModel
    {
        public int VehiclrOrderId { get; set; }
        public int VehiclrOrderStatus { get; set; }

    }
    public class ConfirmProposalIdInVehicleOrder
    {
        public int VehicleOrderId { get; set; }
        public int ProposalId { get; set; }
    }

}