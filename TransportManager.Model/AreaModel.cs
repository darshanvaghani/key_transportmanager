﻿using Newtonsoft.Json;
using System;

namespace TransportManager.Model
{
    public class AreaRequestModel
    {
        public int CityId { get; set; }
        public string SearchText { get; set; }
    }

    public partial class AreaResponceModel
    {
        public long AreaId { get; set; }
        public string AreaName { get; set; }
    }

    public partial class AreaModel
    {
        public int AreaId { get; set; }
        public int StateId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public byte[] ChangeTimeStamp { get; set; }
    }
}
