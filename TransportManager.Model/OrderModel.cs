﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static TransportManager.Common.Constants;

namespace TransportManager.Model
{
    public class AddOrderSummaryModel
    {
        public long OrderSummaryId { get; set; }
        public string OrderNo { get; set; }
        public string OrderTitle { get; set; }
        public string OrderDescription { get; set; }
        public DateTime OrderDate { get; set; }
        [JsonIgnore]
        public string FormatedOrderDate
        {
            get
            {
                return OrderDate.ToString("dd-MM-yyyy");
            }
        }
        public string FromAddress { get; set; }
        public string FromLandmark { get; set; }
        public int FromArea { get; set; }
        public int FromCityId { get; set; }
        public int FromStateId { get; set; }
        public string FromLAT { get; set; }
        public string FromLONG { get; set; }
        public string ToAddress { get; set; }
        public string ToLandmark { get; set; }
        public int ToArea { get; set; }
        public int ToCityId { get; set; }
        public int ToStateId { get; set; }
        public string ToLAT { get; set; }
        public string ToLONG { get; set; }
        public int VehicleTypeId { get; set; }
        public string VehicleType { get; set; }
        public string VehicalTypeImage { get; set; }
        public bool IsShared { get; set; }
        public bool IsLoaded { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public int UserHasProposal { get; set; }
        public int ProposalCount { get; set; }
        public int UserId { get; set; }
        public decimal Price { get; set; }
        public int Status { get; set; }
        public string StatusName
        {
            get
            {
                return ((OrderStatus)Status).ToString();
            }
        }
        public int CreatedById { get; set; }
        //[JsonIgnore]
        //public List<OrderDetailModel> TempProductList
        //{
        //    get
        //    {
        //        if (string.IsNullOrWhiteSpace(ProductJson))
        //        {
        //            return ProductList;
        //        }
        //        else
        //        {
        //            ProductList = JsonConvert.DeserializeObject<List<OrderDetailModel>>(ProductJson);
        //            return ProductList;
        //        }
        //    }
        //}
        public List<AddOrderDetailModel> ProductList { get; set; }
    }

    public class GetOrderSummaryModel : AddOrderSummaryModel
    {
        public new List<GetOrderDetailModel> ProductList { get; set; }
        [JsonIgnore]
        public string ProductJson
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ProductList = JsonConvert.DeserializeObject<List<GetOrderDetailModel>>(value);
                }
            }
        }
    }

    public class GroupedOrderedListData<T>
    {
        public DateTime Date { get; set; }
        public IList<T> OrderList { get; set; }
    }
    public class AddOrderDetailModel
    {
        public long OrderDetailId { get; set; }
        public string ProductName { get; set; }
        public int ProductState { get; set; }
        public int Qty { get; set; }
        public string UoM { get; set; }
        public string Size { get; set; }
        public string SizeType { get; set; }
        public string SizeWidth { get; set; }
        public string SizeHeight { get; set; }
        public decimal Price { get; set; }
        public int MaterialId { get; set; }
    }

    public class GetOrderDetailModel : AddOrderDetailModel
    {
        public string MaterialName { get; set; }
        public string MaterialImage { get; set; }
    }

    public class FeedSearchModel : PaginationModel
    {
        public int City { get; set; }
        public string SearchText { get; set; }
        public int? State { get; set; }
        public int? Area { get; set; }
        public int? FromPrice { get; set; }
        public int? ToPrice { get; set; }
        public bool? IsShared { get; set; }
        public bool? IsLoaded { get; set; }
        public bool? IsHuman { get; set; }
        public int? UserId { get; set; }
    }

    public class GetProductMaterialModel
    {
        public int MaterialId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }

    public class OrderRequestModel : PaginationModel
    {
        public long OrderSummaryId { get; set; }
        public bool? IsHuman { get; set; }
    }

    public class GetOrderByUserIdRequestModel : PaginationModel
    {
        public long UserId { get; set; }
        public bool? IsHuman { get; set; }
    }

    public class PaginationModel
    {
        public int PageNo { get; set; }
        public int PageSize { get; set; }

    }

    public class StatusModel
    {
        public int OrderSummaryId { get; set; }
        public int OrderStatus { get; set; }
    }
    public class ConfirmProposalId
    {
        public int OrderId { get; set; }
        public int ProposalId { get; set; }
    }


}