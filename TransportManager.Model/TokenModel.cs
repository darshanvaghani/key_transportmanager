﻿
namespace TransportManager.Model
{
    public class ValidateTokenRequest
    {
        public string Token { get; set; }
        public long UserId { get; set; }
    }
}
