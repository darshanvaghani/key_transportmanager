﻿using Newtonsoft.Json;
using System;

namespace TransportManager.Model
{
    public class CityRequestModel
    {
        public int StateId { get; set; }
        public string SearchText { get; set; }
    }

    public partial class CityResponceModel
    {
        public long CityId { get; set; }
        public string CityName { get; set; }
    }

    public partial class CityModel
    {
        public int CityId { get; set; }
        public int StateId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public byte[] ChangeTimeStamp { get; set; }
    }
}
