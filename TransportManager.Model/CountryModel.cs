﻿namespace TransportManager.Model
{
	public partial class CountryResponceModel
	{
		public int CountryId { get; set; }
		public string CountryName { get; set; }
	}

	public class CountryModel
	{
		public int CountryId { get; set; }
		public string CountryName { get; set; }
		public string PhoneCode { get; set; }
		public string CurrencySymbol { get; set; }
		public string TaxType { get; set; }
		public string DateFormat { get; set; }
		public bool IsActive { get; set; }
		public bool IsDelete { get; set; }
		public int CreatedBy { get; set; }
		public byte[] ChangeTimeStamp { get; set; }
	}
	
}
