﻿
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TransportManager.Model
{
    public class VehicleProposalModel
    {
        public int VehicleId { get; set; }
        public int UserId { get; set; }
        public int OrderSummaryId { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime? ProposalDateTime { get; set; }
        public int CreatedBy { get; set; }
    }

    public class UpdateVehicleProposalModel
    {
        public int VehicleProposalId { get; set; }
        public int VehicleId { get; set; }
        public int UserId { get; set; }
        public int OrderSummaryId { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime? ProposalDateTime { get; set; }
        public int CreatedBy { get; set; }
    }

    public class GetVehicleProposalModel
    {
        //Vehicle Details
        public int VehicleProposalId { get; set; }
        public int VehicleId { get; set; }
        public int UserId { get; set; }
        public int VehicleTypeId { get; set; }
        public string VehicalType { get; set; }
        public string VehicleNumber { get; set; }
        public string SerialNumber { get; set; }
        public int MaxCapacity { get; set; }
        public string VehicalOrderNo { get; set; }
        public string VehicalOrderDate { get; set; }
        
        public bool IsAdminApprove { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }

        public int OrderSummaryId { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime? ProposalDateTime { get; set; }
        public bool Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        //User Details
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public DateTime? BirthDate { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public int PinCode { get; set; }
        public string DeviceInfo { get; set; }
        public string Image { get; set; }
        public bool IsShared { get; set; }
        public bool IsLoaded { get; set; }
        public string ProfilePercentage { get; set; }
        public string VehicalProof { get; set; }
    }

    public class GetVehicleProposalByUserId : PaginationModel
    {
        public long UserId { get; set; }
    }
    public class UpdateVehicleProposalStatus
    {
        public long VehicleProposalId { get; set; }
        public long Status { get; set; }
        public long UserId { get; set; }
    }

    public class GetVehicleProposalById : PaginationModel
    {
        public int VehicleProposalId { get; set; }
    }
    public class GetVehicleProposalByOrderId : PaginationModel
    {
        public int OrderId { get; set; }
    }
    public class GetVehicleProposalByVehicleId : PaginationModel
    {
        public int VehicleId { get; set; }
    }
    public class GetVehicleProposal : PaginationModel
    {
        public int Id { get; set; }
    }

}
