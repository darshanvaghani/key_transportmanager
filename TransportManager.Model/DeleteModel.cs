﻿using Newtonsoft.Json;
using System;

namespace TransportManager.Model
{
    public class DeleteModel
    {
        public long Id { get; set; }
        public int UserId { get; set; }
    }

}
