﻿
using System;
using System.Collections.Generic;

namespace TransportManager.Model
{
	public class VehicleTypeRequestModel
	{
		public int UserId { get; set; }
		public string VehicleType { get; set; }
		public string Image { get; set; }
		public string ImageBase64 { get; set; }
		public string ImageExtension { get; set; }
	}

	public class UpdateVehicleTypeRequestModel : VehicleTypeRequestModel
	{
		public bool IsActive { get; set; }
	}

	public class VehicleModel
	{
		public int VehicleTypeId { get; set; }
		public string VehicleNumber { get; set; }
		public string SerialNumber { get; set; }
		public int MaxCapacity { get; set; }
		public IList<DocumentDataList> DocumentList { get; set; }
		public int UserId { get; set; }
		public int CreatedBy { get; set; }
	}

	public class DocumentDataList
	{
		public string DocumentBase64 { get; set; }
		public string DocumentExtn { get; set; }
		public int DocumentType { get; set; }
	}

	public class UpdateDocumentDataList : DocumentDataList
	{
		public long DocumentId { get; set; }
	}

	public class DocumentList
	{
		public long DocumentId { get; set; }
		public string Document { get; set; }
		public int DocType { get; set; }
	}

	public class UpdateVehicleModel
	{
		public int VehicleTypeId { get; set; }
		public string VehicleNumber { get; set; }
		public string SerialNumber { get; set; }
		public int MaxCapacity { get; set; }
		public int UserId { get; set; }
		public int CreatedBy { get; set; }
		public IList<UpdateDocumentDataList> DocumentList { get; set; }
	}

	public class GetVehicleModel
	{
		public int VehicleId { get; set; }
		public int UserId { get; set; }
		public int VehicleTypeId { get; set; }
		public string VehicleType { get; set; }
		public string VehicleNumber { get; set; }
		public string SerialNumber { get; set; }
		public string Image { get; set; }
		public int MaxCapacity { get; set; }
		public bool IsAdminApprove { get; set; }
		public bool IsActive { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
	}

	public class GetVehicleTypeModel
	{
		public int VehicleTypeId { get; set; }
		public string VehicleType { get; set; }
		public string Image { get; set; }
	}
	public class GetVehicle : PaginationModel
	{
		public int Id { get; set; }
	}
}
