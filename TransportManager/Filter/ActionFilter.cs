﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TransportManager.Common;
using TransportManager.DAL;
using TransportManager.Model;

namespace TransportManager.Filter
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthFilterAttribute : ActionFilterAttribute, IExceptionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                if (context.HttpContext.Request.Path.Value.ToLower().Substring(0, 4) == "/api")
                {
                    if (!context.ActionDescriptor.EndpointMetadata.Contains(new Microsoft.AspNetCore.Authorization.AllowAnonymousAttribute()))
                    {
                        StringValues token = "";
                        var found = context.HttpContext.Request.Headers.TryGetValue("AuthToken", out token);
                        Dictionary<string, object> dctResult = new Dictionary<string, object>();
                        TranStatus tranStatus = new TranStatus();
                        tranStatus.code = 401;
                        //tranStatus.IsSuccess = false;
                        tranStatus.returnMessage = "You are not authorised"; // You can add 'You are not authorised' Message Here
                        dctResult.Add("Status", tranStatus);
                        if (found && token.ToString().Trim() != "")
                        {
                            using (TokenRepository tokenRepository = new TokenRepository())
                            {
                                ValidateTokenRequest model = new ValidateTokenRequest();
                                string decrptedToken = CommonHelper.Decrypt(token.ToString());
                                model.UserId = Convert.ToInt64(decrptedToken.Substring(36));
                                model.Token = decrptedToken.Substring(0, 36);
                                if (!tokenRepository.ValidateToken(model).Result)
                                {
                                    context.HttpContext.Response.StatusCode = 401;
                                    context.Result = new ObjectResult(dctResult);
                                };
                            }
                        }
                        else
                        {
                            context.HttpContext.Response.StatusCode = 401;
                            context.Result = new ObjectResult(dctResult);
                        }
                    }
                }
                base.OnActionExecuting(context);
            }
            catch (Exception ex)
            {
                base.OnActionExecuting(context);
            }
            finally
            {
            }
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
        }

        public void OnException(ExceptionContext context)
        {
            string ReturnMessage = LogError(context.Exception);
            Dictionary<string, object> dctResult = new Dictionary<string, object>();
            TranStatus tranStatus = new TranStatus();
            //tranStatus.IsSuccess = false;
            tranStatus.returnMessage = "Error Occurred";
            dctResult.Add("Status", tranStatus);
            context.HttpContext.Response.StatusCode = 400;
            context.Result = new ObjectResult(dctResult);
        }

        private string LogError(Exception ex)
        {
            StringBuilder message = new StringBuilder();
            message.Append(string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
            message.Append(Environment.NewLine);
            message.Append("-----------------------------------------------------------");
            message.Append(Environment.NewLine);
            message.Append(string.Format("Message: {0}", ex.Message));
            message.Append(Environment.NewLine);
            message.Append(string.Format("Inner Exception: {0}", ex.InnerException));
            message.Append(Environment.NewLine);
            message.Append(string.Format("StackTrace: {0}", ex.StackTrace));
            message.Append(Environment.NewLine);
            message.Append(string.Format("Source: {0}", ex.Source));
            message.Append(Environment.NewLine);
            message.Append(string.Format("TargetSite: {0}", ex.TargetSite.ToString()));
            message.Append(Environment.NewLine);
            message.Append("-----------------------------------------------------------");
            message.Append(Environment.NewLine);
            string directoryPath = "ExceptionLog\\";
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            string filePath = directoryPath + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(message.ToString());
                writer.Close();
            }
            return message.ToString();
        }

        private string LogForApp(string platformValues, string AppVersionValue, string ApiUrl, string AppUserType)
        {
            StringBuilder message = new StringBuilder();
            message.Append(string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
            message.Append(Environment.NewLine);
            message.Append("-----------------------------------------------------------");
            message.Append(Environment.NewLine);
            message.Append(string.Format("ApiUrl: {0}", ApiUrl));
            message.Append(Environment.NewLine);
            message.Append(string.Format("platform: {0}", platformValues));
            message.Append(Environment.NewLine);
            message.Append(string.Format("AppVersion: {0}", AppVersionValue));
            message.Append(Environment.NewLine);
            message.Append(string.Format("AppUserType: {0}", AppUserType));
            message.Append(Environment.NewLine);
            message.Append("-----------------------------------------------------------");
            message.Append(Environment.NewLine);
            string directoryPath = "\\Documents\\ErrorLog\\";
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            string filePath = directoryPath + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(message.ToString());
                writer.Close();
            }
            return message.ToString();
        }
    }
}
