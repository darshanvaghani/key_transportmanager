using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.Hosting;
using System;
using TransportManager.Common;
using TransportManager.BLL.Interface;
using TransportManager.BLL.Service;
using Microsoft.OpenApi.Models;
using TransportManager.BLL;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.Extensions.FileProviders;

namespace TransportManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllersWithViews();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Transport Manager API", Version = "v1" });
            });
            //services.AddControllersWithViews()
            //   .AddRazorRuntimeCompilation();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(20);
                options.Cookie.HttpOnly = true;
            });

            services.AddControllers()
                .AddNewtonsoftJson(option =>
                {
                    option.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                    option.SerializerSettings.ContractResolver = new DefaultContractResolver();
                });

            // Register the Swagger generator, defining 1 or more Swagger documents
            //services.AddSwaggerGen(c =>
            //{
            //    c.DescribeAllEnumsAsStrings();
            //    c.DescribeAllParametersInCamelCase();
            //    c.SwaggerDoc("v1", new Info { Title = "Transport API", Version = "v1" });
            //});

            //services.AddSignalR()
            //    .AddNewtonsoftJsonProtocol(option =>
            //    {
            //        option.PayloadSerializerSettings.ContractResolver = new DefaultContractResolver();
            //        option.PayloadSerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            //    });


            TransportManagerSettings.ConnectionString = Configuration.GetSection("ConnectionString:TransportEntities").Value;
            DependencyResolver(services);
            services.AddMvc(config =>
            {
                config.Filters.Add(new ProducesResponseTypeAttribute(typeof(TranStatus), (int)HttpStatusCode.BadRequest));
                config.Filters.Add(new ProducesResponseTypeAttribute(typeof(TranStatus), (int)HttpStatusCode.Unauthorized));
                config.Filters.Add(new Filter.AuthFilterAttribute());
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            //app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Transport API V1");
            //});
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Transport Manager Api V1");
                c.RoutePrefix = string.Empty;
            });
            app.UseSession();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //loggerFactory.AddSerilog();
            //app.UseMiddleware();

            //app.UseRequestResponseLoggingMiddleware();
            //app.MapWhen(context => context.Request.Path.Value.Contains("/api/"), appBuilder =>
            //{
            //    appBuilder.UseMiddleware<RequestResponseLoggingMiddleware>();
            //});
            //app.UseMiddleware<RequestResponseLoggingMiddleware>();

            //app.UseMiddleware<ExceptionHandlingMiddleware>();

            //app.UseCors("CorsPolicy");

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(Directory.GetCurrentDirectory(), "Documents")),
                RequestPath = "/Documents"
            });
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapHub<NotifyHub>("/NotifyHub", options =>
                //{
                //    options.Transports = HttpTransportType.WebSockets | HttpTransportType.LongPolling;
                //});
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            TransportManagerSettings.ImagePathUrl = Directory.GetCurrentDirectory();
            // Based on configuration connect to hub or create own dependency with database for required table
            //if (Convert.ToBoolean(Configuration.GetSection("Hub:ConnectToHub").Value))
            //{
            //    app.UseSqlTableDependency<DatabaseSubscriptionClient>(Configuration.GetSection("Hub:ConnectionUrl").Value);
            //}
            //else
            //{
            //app.UseSqlTableDependency<RoundSummaryTableDependency>(BattingSettings.ConnectionString);
            //}
        }

        private void DependencyResolver(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IUser, UserService>();
            services.AddSingleton<ICountry, CountryService>();
            services.AddSingleton<IState, StateService>();
            services.AddSingleton<ICity, CityService>();
            services.AddSingleton<IArea, AreaService>();
            services.AddSingleton<IOrder, OrderService>();
            services.AddSingleton<IVehicle, VehicleService>();
            services.AddSingleton<IVehicleProposal, VehicleProposalService>();
            services.AddSingleton<IVehicleOrder, VehicleOrderService>();
            services.AddSingleton<IUserProposal, UserProposalService>();
        }
    }
}
