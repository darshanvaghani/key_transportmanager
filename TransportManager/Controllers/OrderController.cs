﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;
using static TransportManager.Common.Constants;

namespace TransportManager.Controllers
{
	[Route("api/[controller]")]
	public class OrderController : Controller
	{
		public IOrder iOrder { get; set; }

		public OrderController([FromServices] IOrder order)
		{
			iOrder = order;
		}

		[HttpPost]
		[Route("GetOrder")]
		public async Task<IActionResult> GetOrderAsync([FromBody]OrderRequestModel requestModel)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var list = await iOrder.GetOrderAsync(requestModel);
				var groupedData = list.Item1.GroupBy(x => x.FormatedOrderDate).Select(x => new GroupedOrderedListData<GetOrderSummaryModel>() { Date = x.FirstOrDefault().OrderDate, OrderList = x.ToList() });
				dctData.Add("AllOrders", groupedData);
				dctData.Add("TotalDataCount", list.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[Route("AddUpdateOrder")]
		[HttpPost]
		public async Task<IActionResult> AddUpdateOrder([FromBody]AddOrderSummaryModel model)
		{
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			TranStatus transaction;
			try
			{
				transaction = await iOrder.AddUpdateOrderAsync(model);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route("DeleteOrder")]
		public async Task<IActionResult> DeleteOrder(DeleteModel model)
		{
			TranStatus transaction;
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				transaction = await iOrder.DeleteOrderAsync(model);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpGet]
		[Route("GetOrderStatus")]
		public async Task<IActionResult> GetOrderStatusAsync()
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var statusList = CommonHelper.SelectListFor(typeof(Constants.OrderStatus));
				dctData.Add("OrderStatus", statusList);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpGet]
		[Route("GetProductState")]
		public IActionResult GetProductStateAsync()
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var statusList = CommonHelper.SelectListFor(typeof(Constants.ProductState));
				dctData.Add("ProductState", statusList);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route("GetOrderByUserId")]
		public async Task<IActionResult> GetOrderByUserIdAsync([FromBody]GetOrderByUserIdRequestModel requestModel)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var list = await iOrder.GetOrderByUserIdAsync(requestModel);
				var groupedData = list.Item1.GroupBy(x => x.FormatedOrderDate).Select(x => new GroupedOrderedListData<GetOrderSummaryModel>() { Date = x.FirstOrDefault().OrderDate, OrderList = x.ToList() });
				dctData.Add("AllOrder", groupedData);
				dctData.Add("TotalDataCount", list.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}


		[HttpPost]
		[Route("GetRunningOrderByUserId")]
		public async Task<IActionResult> GetRunningOrderByUserId([FromBody]GetOrderByUserIdRequestModel requestModel)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var list = await iOrder.GetRunningOrderByUserId(requestModel);
				var groupedData = list.Item1.GroupBy(x => x.FormatedOrderDate).Select(x => new GroupedOrderedListData<GetOrderSummaryModel>() { Date = x.FirstOrDefault().OrderDate, OrderList = x.ToList() });
				dctData.Add("AllOrder", groupedData);
				dctData.Add("TotalDataCount", list.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route("GetFeedForVehicle")]
		public async Task<IActionResult> GetFeedForVehicleAsync([FromBody]FeedSearchModel model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var list = await iOrder.GetFeedForVehicleAsync(model);
				var groupedData = list.Item1.GroupBy(x => x.FormatedOrderDate).Select(x => new GroupedOrderedListData<GetOrderSummaryModel>() { Date = x.FirstOrDefault().OrderDate, OrderList = x.ToList() });
				dctData.Add("AllOrder", groupedData);
				dctData.Add("TotalDataCount", list.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route("ChangeStatus/{orderid}")]
		public async Task<IActionResult> ChangeOrderStatus(int orderid, [FromBody]ChangeStatusModel model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var list = await iOrder.ChangeOrderStatus(orderid, model);
				dctData.Add("OrderList", list);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpGet]
		[Route(nameof(GetProductMaterial))]
		public async Task<IActionResult> GetProductMaterial()
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var response = await iOrder.GetProductMaterial();
				dctData.Add("ProductMaterial", response);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[Route("StatusUpdate")]
		[HttpPost]
		public async Task<IActionResult> StatusUpdate([FromBody]StatusModel model)
		{
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			TranStatus transaction;
			try
			{
				//var status = Enum.GetName(typeof(OrderStatus) ,model.OrderStatus);
				transaction = await iOrder.StatusUpdate(model);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route("ConfirmProposalInOrder")]
		public async Task<IActionResult> ConfirmProposalInOrder([FromBody]ConfirmProposalId model)
		{
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			TranStatus transaction;
			try
			{
				//var status = Enum.GetName(typeof(OrderStatus) ,model.OrderStatus);
				transaction = await iOrder.ConfirmProposalInOrder(model);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}
	}
}