﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.Controllers
{
	[Route("api/[controller]")]
	public class CountryController : Controller
	{
		public ICountry iCountry { get; set; }

		public CountryController([FromServices] ICountry country)
		{
			iCountry = country;
		}

		[Route(nameof(GetCountry))]
		[AllowAnonymous]
		[HttpPost]
		public async Task<IActionResult> GetCountry()
		{
			TranStatus transaction = null;
			Dictionary<String, Object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var country = await iCountry.GetCountryAsync();
				dctData.Add("Country", country);
			}
			catch (Exception ex)
			{
				transaction = new TranStatus();
				transaction.code = Constants.Status.Error;
				transaction.returnMessage = ex.Message;
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		//[Route(nameof(AddCountry))]
		//[HttpPost]
		//public IActionResult AddCountry([FromBody]CountryModel itemModel)
		//{
		//    TranStatus transaction = null;
		//    Dictionary<String, Object> dctData = new Dictionary<string, object>();
		//    HttpStatusCode statusCode = HttpStatusCode.OK;
		//    try
		//    {
		//        transaction = iCountry.AddCountry(itemModel);
		//    }
		//    catch (Exception ex)
		//    {
		//        transaction = new TranStatus();
		//        transaction.code = Constants.Status.Error;
		//        transaction.returnMessage = ex.Message;
		//        statusCode = HttpStatusCode.BadRequest;
		//    }
		//    dctData.Add("Status", transaction);
		//    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		//}

		//[Route(nameof(UpdateCountry))]
		//[HttpPost]
		//public IActionResult UpdateCountry([FromBody]CountryModel itemModel)
		//{
		//    TranStatus transaction = null;
		//    Dictionary<String, Object> dctData = new Dictionary<string, object>();
		//    HttpStatusCode statusCode = HttpStatusCode.OK;
		//    try
		//    {
		//        transaction = iCountry.UpdateCountry(itemModel);
		//    }
		//    catch (Exception ex)
		//    {
		//        transaction = new TranStatus();
		//        transaction.code = Constants.Status.Error;
		//        transaction.returnMessage = ex.Message;
		//        statusCode = HttpStatusCode.BadRequest;
		//    }
		//    dctData.Add("Status", transaction);
		//    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		//}

		//[Route(nameof(DeleteCountry))]
		//[HttpPost]
		//public IActionResult DeleteCountry([FromBody]CountryModel itemModel)
		//{
		//    TranStatus transaction = null;
		//    Dictionary<String, Object> dctData = new Dictionary<string, object>();
		//    HttpStatusCode statusCode = HttpStatusCode.OK;
		//    try
		//    {
		//        transaction = iCountry.DeleteCountry(itemModel);
		//    }
		//    catch (Exception ex)
		//    {
		//        transaction = new TranStatus();
		//        transaction.code = Constants.Status.Error;
		//        transaction.returnMessage = ex.Message;
		//        statusCode = HttpStatusCode.BadRequest;
		//    }
		//    dctData.Add("Status", transaction);
		//    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		//}
	}
}