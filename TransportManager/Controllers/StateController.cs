﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.Controllers
{
    [Route("api/[controller]")]
    public class StateController : Controller
    {
        public IState iState { get; set; }

        public StateController([FromServices] IState warehouse)
        {
            iState = warehouse;
        }

        [Route("GetState")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> GetState([FromBody]StateRequestModel itemModel)
        {
            TranStatus transaction = null;
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var itemList = await iState.GetState(itemModel);
                dctData.Add("State", itemList);
            }
            catch (Exception ex)
            {
                transaction = new TranStatus();
                transaction.code = Constants.Status.Error;
                transaction.returnMessage = ex.Message;
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

       
        //[Route("AddState")]
        //[HttpPost]
        //public IActionResult AddState([FromBody]StateModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iState.AddState(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}

        //[Route("UpdateState")]
        //[HttpPost]
        //public IActionResult UpdateState([FromBody]StateModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iState.UpdateState(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}

        //[Route("DeleteState")]
        //[HttpPost]
        //public IActionResult DeleteState([FromBody]StateModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iState.DeleteState(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}
    }
}