using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;
using UAParser;

namespace TransportManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUser iUser;
        public UserController(IUser user)
        {
            iUser = user;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody]LoginRequestModel model)
        {
            Dictionary<string, dynamic> dctData = new Dictionary<string, dynamic>();
            TranStatus transaction = new TranStatus();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                if (model != null)
                {
                    if (model.UserName.Trim() != "" && model.Password.Trim() != "")
                    {
                        model.Password = CommonHelper.Encrypt(model.Password);
                        var data = await iUser.CheckLoginUser(model);
                        LoginResponseModel user = data.Item1;
                        transaction = data.Item2;
                        if (user != null)
                        {
                            if (user.Token != null && user.UserId != 0)
                            {
                                string reGenaretedToken = user.Token + user.UserId.ToString();
                                user.Token = CommonHelper.Encrypt(reGenaretedToken);
                            }
                            dctData.Add("UserData", user);
                        }
                    }
                    else
                    {
                        transaction.code = Constants.Status.Error;
                        transaction.returnMessage = "Invalid credentials";
                    }
                }
                else
                {
                    transaction.code = Constants.Status.Error;
                    transaction.returnMessage = "Please specify username and password";
                }
            }
            catch (Exception ex)
            {
                transaction = new TranStatus();
                transaction.returnMessage = ex.Message;
                transaction.code = Constants.Status.Error;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody]RegisterModel objUser)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                if (!string.IsNullOrEmpty(objUser.Password)) { 
                    objUser.Password = CommonHelper.Encrypt(objUser.Password);
                }

                var data = await iUser.Register(objUser);
                LoginResponseModel user = data.Item1;
                transaction = data.Item2;
                if (user != null)
                {
                    if (user.Token != null && user.UserId != 0)
                    {
                        string reGenaretedToken = user.Token + user.UserId.ToString();
                        user.Token = CommonHelper.Encrypt(reGenaretedToken);
                    }
                    dctData.Add("UserData", user);
                }
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPut]
        [Route("UpdateUser/{id}")]
        public async Task<IActionResult> UpdateUser(long id, [FromBody]UpdateUserModel objUser)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUser.UpdateUser(objUser, id);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route("Logout/{id}")]
        public async Task<IActionResult> Logout(long id)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUser.Logout(id);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPut]
        [Route("ChangePassword/{id}")]
        public async Task<IActionResult> UpdatePassword(long id, [FromBody]ChangePasswordRequestModel changePassword)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                changePassword.OldPassword = CommonHelper.Encrypt(changePassword.OldPassword);
                changePassword.NewPassword = CommonHelper.Encrypt(changePassword.NewPassword);
                transaction = await iUser.ChangePassword(id, changePassword);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPut]
        [Route("ChangeStatus/{id}")]
        public async Task<IActionResult> ChangeStatus(long id, [FromBody]ChangeStatusModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUser.ChangeStatus(id, model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpGet]
        [Route("GetLoginHistory/{id}")]
        public async Task<IActionResult> GetLoginHistory(long id)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var loginHistory = await iUser.GetLoginHistory(id);
                dctData.Add("LoginHistory", loginHistory);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(long userId)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUser.Delete(userId);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpGet]
        [Route("GetUser/{id}")]
        public async Task<IActionResult> GetUser(long id)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var loginHistory = await iUser.GetUser(id);
                dctData.Add("User", loginHistory);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }


        [HttpGet]
        [Route("GetRole")]
        public async Task<IActionResult> GetRole()
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var statusList = await iUser.GetRole();
                //var statusList = CommonHelper.SelectListFor(typeof(Constants.UserRole));
                dctData.Add("UserRole", statusList);
            }
            catch (Exception ex)
            { 
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }        
    }
}
