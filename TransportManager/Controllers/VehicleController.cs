using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;
using UAParser;

namespace TransportManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicle iVehicle;
        public VehicleController(IVehicle vehicle)
        {
            iVehicle = vehicle;
        }

        [HttpPost]
        [Route(nameof(AddVehicleType))]
        public async Task<IActionResult> AddVehicleType([FromBody]VehicleTypeRequestModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {

                if (!string.IsNullOrWhiteSpace(model.ImageBase64))
                {
                    model.Image = CommonHelper.SaveFile("VehicleType", model.ImageBase64, model.ImageExtension);
                }

                transaction = await iVehicle.AddVehicleType(model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPut]
        [Route(nameof(UpdateVehicleType) + "/{id}")]
        public async Task<IActionResult> UpdateVehicleType(int id, [FromBody]UpdateVehicleTypeRequestModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                if (!string.IsNullOrWhiteSpace(model.ImageBase64))
                {
                    model.Image = CommonHelper.SaveFile("VehicleType", model.ImageBase64, model.ImageExtension);
                }
                transaction = await iVehicle.UpdateVehicleType(id, model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(AddVehicle))]
        public async Task<IActionResult> AddVehicle([FromBody]VehicleModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                IList<DocumentList> docList = new List<DocumentList>();
                if (model.DocumentList != null && model.DocumentList.Count > 0)
                {
                    foreach (DocumentDataList item in model.DocumentList)
                        if (!string.IsNullOrWhiteSpace(item.DocumentBase64) && !string.IsNullOrWhiteSpace(item.DocumentExtn))
                        {
                            if (item.DocumentType != 0)
                            {
                                docList.Add(new DocumentList()
                                {
                                    DocumentId = 0,
                                    Document = CommonHelper.SaveFile("VehicleDocument", item.DocumentBase64, item.DocumentExtn),
                                    DocType = item.DocumentType
                                });
                            }
                            else
                            {
                                throw new Exception("Document Type is not null or Zero.");
                            }
                        }

                }
                transaction = await iVehicle.AddVehicle(model, docList);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPut]
        [Route(nameof(UpdateVehicle) + "/{id}")]
        public async Task<IActionResult> UpdateVehicle(int id, [FromBody]UpdateVehicleModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                IList<DocumentList> documentLists = new List<DocumentList>();
                if (model.DocumentList != null && model.DocumentList.Count > 0)
                {
                    foreach (UpdateDocumentDataList item in model.DocumentList)
                        if (!string.IsNullOrWhiteSpace(item.DocumentBase64) && !string.IsNullOrWhiteSpace(item.DocumentExtn))
                        {
                            if (item.DocumentType != 0)
                            {
                                documentLists.Add(new DocumentList()
                                {
                                    DocumentId = 0,
                                    Document = CommonHelper.SaveFile("VehicleDocument", item.DocumentBase64, item.DocumentExtn),
                                    DocType = item.DocumentType
                                });
                            }
                            else
                            {
                                throw new Exception("Document Type is not null or Zero.");
                            }
                        }
                        else
                        {
                            if (item.DocumentId != 0)
                            {
                                documentLists.Add(new DocumentList()
                                {
                                    DocumentId = item.DocumentId,
                                    Document = "",
                                    DocType = item.DocumentType
                                });
                            }
                            else
                            {
                                throw new Exception("Document Type is not null or Zero.");
                            }
                        }

                }
                transaction = await iVehicle.UpdateVehicle(id, model, documentLists);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPut]
        [Route(nameof(ChangeVehicleStatus) + "/{id}")]
        public async Task<IActionResult> ChangeVehicleStatus(int id, [FromBody]ChangeStatusModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iVehicle.ChangeVehicleStatus(id, model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpDelete]
        [Route(nameof(DeleteVehicleType))]
        public async Task<IActionResult> DeleteVehicleType([FromQuery(Name = "VehicleTypeId")] int id, [FromQuery(Name = "UserId")] int createdById)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iVehicle.DeleteVehicleType(id, createdById);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpDelete]
        [Route(nameof(DeleteVehicle))]
        public async Task<IActionResult> DeleteVehicle([FromQuery(Name = "VehicleId")] int id, [FromQuery(Name = "UserId")] int createdById)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iVehicle.DeleteVehicle(id, createdById);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpGet]
        [Route(nameof(GetVehicleType))]
        public async Task<IActionResult> GetVehicleType()
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var response = await iVehicle.GetVehicleType();
                dctData.Add("VehicleType", response);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(GetVehicle))]
        public async Task<IActionResult> GetVehicle([FromBody]GetVehicle model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var VehicleList = await iVehicle.GetVehicle(model);
                dctData.Add("Vehicle", VehicleList.Item1);
                dctData.Add("TotalDataCount", VehicleList.Item2);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(GetVehicleByUserId))]
        public async Task<IActionResult> GetVehicleByUserId([FromBody]GetVehicle model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var VehicleList = await iVehicle.GetVehicleByUserId(model);
                dctData.Add("Vehicle", VehicleList.Item1);
                dctData.Add("TotalDataCount", VehicleList.Item2);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }
    }
}
