﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.Controllers
{
    [Route("api/[controller]")]
    public class AreaController : Controller
    {
        public IArea iArea { get; set; }

        public AreaController([FromServices] IArea area)
        {
            iArea = area;
        }

        [Route("GetArea")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> GetArea([FromBody]AreaRequestModel itemModel)
        {
            TranStatus transaction = null;
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var itemList = await iArea.GetAreaAsync(itemModel);
                dctData.Add("Area", itemList);
            }
            catch (Exception ex)
            {
                transaction = new TranStatus();
                transaction.code = Constants.Status.Error;
                transaction.returnMessage = ex.Message;
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }
       
        //[Route("AddArea")]
        //[HttpPost]
        //public IActionResult AddArea([FromBody]AreaModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iArea.AddArea(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}

        //[Route("UpdateArea")]
        //[HttpPost]
        //public IActionResult UpdateArea([FromBody]AreaModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iArea.UpdateArea(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}

        //[Route("DeleteArea")]
        //[HttpPost]
        //public IActionResult DeleteArea([FromBody]AreaModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iArea.DeleteArea(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}
    }
}