using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;
using UAParser;

namespace TransportManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProposalController : ControllerBase
    {
        private readonly IUserProposal iUserProposal;
        public UserProposalController(IUserProposal userProposal)
        {
            iUserProposal = userProposal;
        }

        [HttpPost]
        [Route(nameof(AddUserProposal))]
        public async Task<IActionResult> AddUserProposal([FromBody]UserProposalModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUserProposal.AddUserProposal(model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPut]
        [Route(nameof(UpdateUserProposal) + "/{id}")]
        public async Task<IActionResult> UpdateUserProposal(int id, [FromBody]UpdateUserProposalModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUserProposal.UpdateUserProposal(id, model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPut]
        [Route(nameof(ChangeUserProposalStatus))]
        public async Task<IActionResult> ChangeUserProposalStatus([FromBody]ChangeUserProposalStatusModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUserProposal.ChangeUserProposalStatus(model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(UpdateUserProposalStatus))]
        public async Task<IActionResult> UpdateUserProposalStatus(UpdateUserProposalStatus model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUserProposal.UpdateUserProposalStatus(model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpDelete]
        [Route(nameof(DeleteUserProposal))]
        public async Task<IActionResult> DeleteUserProposal([FromQuery(Name = "UserProposalId")] int id, [FromQuery(Name = "UserId")] int createdById)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iUserProposal.DeleteUserProposal(id, createdById);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(GetAllUserProposal))]
        public async Task<IActionResult> GetAllUserProposal([FromBody]PaginationModel model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var VehicleProposalList = await iUserProposal.GetAllUserProposal(model);
                dctData.Add("UserProposal", VehicleProposalList.Item1);
                dctData.Add("TotalDataCount", VehicleProposalList.Item2);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(GetUserProposalByUserId))]
        public async Task<IActionResult> GetUserProposalByUserId([FromBody]GetUserProposalByUserId model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var VehicleProposalList = await iUserProposal.GetUserProposalByUserId(model);
                dctData.Add("UserProposal", VehicleProposalList.Item1);
                dctData.Add("TotalDataCount", VehicleProposalList.Item2);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);

            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        //[Route(nameof(GetVehicleProposalById) + "/{VehicleProposalId}")]
        [Route(nameof(GetUserProposalById))]
        public async Task<IActionResult> GetUserProposalById([FromBody]GetUserProposalById model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var VehicleProposalList = await iUserProposal.GetUserProposalById(model);

                dctData.Add("UserProposal", VehicleProposalList.Item1);
                dctData.Add("TotalDataCount", VehicleProposalList.Item2);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(GetUserProposalByOrderId))]
        public async Task<IActionResult> GetUserProposalByOrderId([FromBody]GetUserProposalByVehicleOrderId model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var VehicleProposalList = await iUserProposal.GetUserProposalByVehicleOrderId(model);
                dctData.Add("UserProposal", VehicleProposalList.Item1);
                dctData.Add("TotalDataCount", VehicleProposalList.Item2);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }
    }
}
