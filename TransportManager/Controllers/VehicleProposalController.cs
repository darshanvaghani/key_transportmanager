using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;
using UAParser;

namespace TransportManager.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class VehicleProposalController : ControllerBase
	{
		private readonly IVehicleProposal iVehicleProposal;
		public VehicleProposalController(IVehicleProposal vehicleProposal)
		{
			iVehicleProposal = vehicleProposal;
		}

		[HttpPost]
		[Route(nameof(AddVehicleProposal))]
		public async Task<IActionResult> AddVehicleProposal([FromBody]VehicleProposalModel model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<String, Object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				transaction = await iVehicleProposal.AddVehicleProposal(model);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPut]
		[Route(nameof(UpdateVehicleProposal) + "/{id}")]
		public async Task<IActionResult> UpdateVehicleProposal(int id, [FromBody]UpdateVehicleProposalModel model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<String, Object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				transaction = await iVehicleProposal.UpdateVehicleProposal(id, model);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPut]
		[Route(nameof(ChangeVehicleProposalStatus) + "/{id}")]
		public async Task<IActionResult> ChangeVehicleProposalStatus(int id, [FromBody]ChangeStatusModel model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<String, Object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				transaction = await iVehicleProposal.ChangeVehicleProposalStatus(id, model);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route(nameof(UpdateVehicleProposalStatus))]
		public async Task<IActionResult> UpdateVehicleProposalStatus(UpdateVehicleProposalStatus model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<String, Object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				transaction = await iVehicleProposal.UpdateVehicleProposalStatus(model);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpDelete]
		[Route(nameof(DeleteVehicleProposal))]
		public async Task<IActionResult> DeleteVehicleProposal([FromQuery(Name = "VehicleProposalId")] int id, [FromQuery(Name = "UserId")] int createdById)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				transaction = await iVehicleProposal.DeleteVehicleProposal(id, createdById);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route(nameof(GetAllVehicleProposal))]
		public async Task<IActionResult> GetAllVehicleProposal([FromBody]PaginationModel model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var VehicleProposalList = await iVehicleProposal.GetAllVehicleProposal(model);
				dctData.Add("VehicleProposal", VehicleProposalList.Item1);
				dctData.Add("TotalDataCount", VehicleProposalList.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route(nameof(GetVehicleProposalByUserId))]
		public async Task<IActionResult> GetVehicleProposalByUserId([FromBody]GetVehicleProposalByUserId model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var VehicleProposalList = await iVehicleProposal.GetVehicleProposalByUserId(model);
				dctData.Add("VehicleProposal", VehicleProposalList.Item1);
				dctData.Add("TotalDataCount", VehicleProposalList.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		//[Route(nameof(GetVehicleProposalById) + "/{VehicleProposalId}")]
		[Route(nameof(GetVehicleProposalById))]
		public async Task<IActionResult> GetVehicleProposalById([FromBody]GetVehicleProposalById model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var VehicleProposalList = await iVehicleProposal.GetVehicleProposalById(model);

                dctData.Add("VehicleProposal", VehicleProposalList.Item1);
				dctData.Add("TotalDataCount", VehicleProposalList.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route(nameof(GetVehicleProposalByOrderId))]
		public async Task<IActionResult> GetVehicleProposalByOrderId([FromBody]GetVehicleProposalByOrderId model)
		{
            GetVehicleProposalModel modelData = new GetVehicleProposalModel();
            TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var VehicleProposalList = await iVehicleProposal.GetVehicleProposalByOrderId(model);
                dctData.Add("VehicleProposal", VehicleProposalList.Item1);
				dctData.Add("TotalDataCount", VehicleProposalList.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

		[HttpPost]
		[Route(nameof(GetVehicleProposalByVehicleId))]
		public async Task<IActionResult> GetVehicleProposalByVehicleId([FromBody]GetVehicleProposalByVehicleId model)
		{
			TranStatus transaction = new TranStatus();
			Dictionary<string, object> dctData = new Dictionary<string, object>();
			HttpStatusCode statusCode = HttpStatusCode.OK;
			try
			{
				var VehicleProposalList = await iVehicleProposal.GetVehicleProposalByVehicleId(model);
				dctData.Add("VehicleProposal", VehicleProposalList.Item1);
				dctData.Add("TotalDataCount", VehicleProposalList.Item2);
			}
			catch (Exception ex)
			{
				transaction = CommonHelper.TransactionErrorHandler(ex);
				statusCode = HttpStatusCode.BadRequest;
			}
			dctData.Add("Status", transaction);
			return this.StatusCode(Convert.ToInt32(statusCode), dctData);
		}

	}
}
