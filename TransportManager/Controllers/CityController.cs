﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.Controllers
{
    [Route("api/[controller]")]
    public class CityController : Controller
    {
        public ICity iCity { get; set; }

        public CityController([FromServices] ICity city)
        {
            iCity = city;
        }

        [Route("GetCity")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> GetCity([FromBody]CityRequestModel itemModel)
        {
            TranStatus transaction = null;
            Dictionary<String, Object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var itemList = await iCity.GetCityAsync(itemModel);
                dctData.Add("City", itemList);
            }
            catch (Exception ex)
            {
                transaction = new TranStatus();
                transaction.code = Constants.Status.Error;
                transaction.returnMessage = ex.Message;
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        //[Route("AddCity")]
        //[HttpPost]
        //public IActionResult AddCity([FromBody]CityModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iCity.AddCity(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}

        //[Route("UpdateCity")]
        //[HttpPost]
        //public IActionResult UpdateCity([FromBody]CityModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iCity.UpdateCity(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}

        //[Route("DeleteCity")]
        //[HttpPost]
        //public IActionResult DeleteCity([FromBody]CityModel itemModel)
        //{
        //    TranStatus transaction = null;
        //    Dictionary<String, Object> dctData = new Dictionary<string, object>();
        //    HttpStatusCode statusCode = HttpStatusCode.OK;
        //    try
        //    {
        //        transaction = iCity.DeleteCity(itemModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        transaction = new TranStatus();
        //        transaction.code = Constants.Status.Error;
        //        transaction.returnMessage = ex.Message;
        //        statusCode = HttpStatusCode.BadRequest;
        //    }
        //    dctData.Add("Status", transaction);
        //    return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        //}
    }
}