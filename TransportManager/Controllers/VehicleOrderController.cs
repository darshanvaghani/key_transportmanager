﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TransportManager.BLL.Interface;
using TransportManager.Common;
using TransportManager.Model;

namespace TransportManager.Controllers
{
    [Route("api/[controller]")]
    public class VehicleOrderController : Controller
    {
        public IVehicleOrder iVehicleOrder { get; set; }

        public VehicleOrderController([FromServices] IVehicleOrder vehicleOrder)
        {
            iVehicleOrder = vehicleOrder;
        }

        [Route(nameof(AddUpdateVehicleOrder))]
        [HttpPost]
        public async Task<IActionResult> AddUpdateVehicleOrder([FromBody]VehicleOrderSummaryModel model)
        {
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            TranStatus transaction;
            try
            {
                transaction = await iVehicleOrder.AddUpdateVehicleOrder(model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(DeleteVehicleOrder))]
        public async Task<IActionResult> DeleteVehicleOrder(DeleteModel model)
        {
            TranStatus transaction;
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iVehicleOrder.DeleteVehicleOrder(model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route("ChangeStatus/{vehicleordersummaryid}")]
        public async Task<IActionResult> ChangeVehicleOrderStatus(int vehicleordersummaryid, ChangeStatusModel model)
        {
            TranStatus transaction;
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                transaction = await iVehicleOrder.ChangeVehicleOrderStatus(vehicleordersummaryid, model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route(nameof(GetFeedForUser))]
        public async Task<IActionResult> GetFeedForUser([FromBody]GetVehicleOrder model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var list = await iVehicleOrder.GetVehicleOrder(model);
                var groupedData = list.Item1.GroupBy(x => x.FormatedOrderDate).Select(x => new GroupedOrderedListData<VehicleOrderSummaryDetails>() { Date = x.FirstOrDefault().VehicleOrderDate, OrderList = x.ToList() });
                dctData.Add("AllOrders", groupedData);
                dctData.Add("TotalDataCount", list.Item2);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route("GetVehicleOrderById")]
        public async Task<IActionResult> GetVehicleOrderById([FromBody]GetVehicleOrder model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var list = await iVehicleOrder.GetVehicleOrderById(model);
                dctData.Add("VehicleOrder", list);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route("GetVehicleOrderByUserId")]
        public async Task<IActionResult> GetVehicleOrderByUserId([FromBody]GetVehicleOrder model)
        {
            TranStatus transaction = new TranStatus();
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            try
            {
                var list = await iVehicleOrder.GetVehicleOrderByUserId(model);
                var groupedData = list.Item1.GroupBy(x => x.FormatedOrderDate).Select(x => new GroupedOrderedListData<VehicleOrderSummaryDetails>() { Date = x.FirstOrDefault().VehicleOrderDate, OrderList = x.ToList() });
                dctData.Add("AllOrders", groupedData);
                dctData.Add("TotalDataCount", list.Item2);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [Route("StatusUpdate")]
        [HttpPost]
        public async Task<IActionResult> StatusUpdate([FromBody]VehicleOrderStatusUpdateModel model)
        {
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            TranStatus transaction;
            try
            {
                //var status = Enum.GetName(typeof(OrderStatus) ,model.OrderStatus);
                transaction = await iVehicleOrder.StatusUpdate(model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }

        [HttpPost]
        [Route("ConfirmProposalInVehicleOrder")]
        public async Task<IActionResult> ConfirmProposalInVehicleOrder([FromBody]ConfirmProposalIdInVehicleOrder model)
        {
            Dictionary<string, object> dctData = new Dictionary<string, object>();
            HttpStatusCode statusCode = HttpStatusCode.OK;
            TranStatus transaction;
            try
            {
                //var status = Enum.GetName(typeof(OrderStatus) ,model.OrderStatus);
                transaction = await iVehicleOrder.ConfirmProposalInVehicleOrder(model);
            }
            catch (Exception ex)
            {
                transaction = CommonHelper.TransactionErrorHandler(ex);
                statusCode = HttpStatusCode.BadRequest;
            }
            dctData.Add("Status", transaction);
            return this.StatusCode(Convert.ToInt32(statusCode), dctData);
        }
    }
}