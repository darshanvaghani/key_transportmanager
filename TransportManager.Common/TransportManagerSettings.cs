﻿using System;

namespace TransportManager.Common
{
    public static class TransportManagerSettings
    {
        public static string ConnectionString = "";
        public static string ImagePathUrl = "";
    }
}
