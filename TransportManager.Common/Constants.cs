﻿namespace TransportManager.Common
{
    public class Constants
    {
        public static class Status
        {
            public static int Success { get { return 0; } }
            public static int Warning { get { return 1; } }
            public static int Error { get { return 2; } }
            public static int Info { get { return 3; } }
        }

        public enum UserRole
        {
            Admin = 1,
            VehicleOwner = 2,
            Customer = 3
        }

        public enum OrderStatus
        {
            Open = 1,
            Pending = 2,
            Confirm = 3,
            Close = 4,
            Cancel = 5
        }

        public enum ProductState
        {
            Solid = 1,
            Liquid = 2,
            Gas = 3,
            Aerosol = 4,
            Human = 5
        }

        public enum RefrenceTable
        {
            User = 1,
            Vehicle = 2
        }

        public enum DocumentType
        {
            RegistrationCertificateBook = 1,
            PUC = 2,
            VehicleImage = 3,
            InsurancePolicy = 4,
            RoadWorthiness = 5,
            AnyOtherRequiredPermits = 6,
        }

        public enum ProposalStatus
        {
            Pending = 1,
            Confirm = 2,
            Decline = 3,
            InProgress = 4,
            Done = 5,
            Cancel = 6
        }
    }
    public class TranStatus
    {
        public string returnMessage { get; set; }
        public int code { get; set; }
        public TranStatus()
        {
            returnMessage = "";
            code = 0;
        }
    }
}
