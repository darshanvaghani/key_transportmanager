﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Reflection;

namespace TransportManager.Common
{
    public class CommonHelper
    {
        public static void WriteToFile(string Message, string path = "", string filename = null)
        {
            string basePath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs" + path;
            //Message = "[" + DateTime.UtcNow + "] Log : " + Message;
            if (!Directory.Exists(basePath))
            {
                Directory.CreateDirectory(basePath);
            }
            string tmpFileName = filename == null || filename == "" ? DateTime.Now.Date.ToShortDateString().Replace('/', '.')
                : filename + "_" + DateTime.Now.Date.ToShortDateString().Replace('/', '.');
            string filepath = basePath + "\\" + tmpFileName + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        #region  Encrypt and Decrypt
        public static string Encrypt(string strText)
        {
            if (strText == null || strText == "")
            {
                return strText;
            }
            else
            {
                byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };
                try
                {
                    string strEncrKey = "&%#@?,:*";
                    byte[] bykey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));
                    byte[] inputByteArray = System.Text.Encoding.UTF8.GetBytes(strText);
                    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                    MemoryStream ms = new MemoryStream();
                    CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(bykey, IV), CryptoStreamMode.Write);
                    cs.Write(inputByteArray, 0, inputByteArray.Length);
                    cs.FlushFinalBlock();
                    return Convert.ToBase64String(ms.ToArray());
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }

        public static string Decrypt(string strText)
        {
            if (strText == null || strText == "")
            {
                return strText;
            }
            else
            {
                byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };
                byte[] inputByteArray = new byte[strText.Length + 1];
                try
                {
                    string strEncrKey = "&%#@?,:*";
                    byte[] byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));
                    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                    inputByteArray = Convert.FromBase64String(strText);
                    MemoryStream ms = new MemoryStream();
                    CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);
                    cs.Write(inputByteArray, 0, inputByteArray.Length);
                    cs.FlushFinalBlock();
                    System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                    return encoding.GetString(ms.ToArray());
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }
        #endregion

        public static TranStatus TransactionErrorHandler(Exception ex)
        {

            TranStatus transaction = new TranStatus();
            if (ex.Message.Contains("||"))
            {
                transaction.code = Convert.ToInt32(ex.Message.Split("||")[0]);
                transaction.returnMessage = ex.Message.Split("||")[1];
            }
            else
            {
                transaction.code = Constants.Status.Error;
                transaction.returnMessage = ex.Message;
            }
            return transaction;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

        public static IList<KeyValuePair<string, int>> SelectListFor(Type enumType)
        {
            if (enumType.IsEnum)
            {
                return Enum.GetValues(enumType).Cast<int>().Select(e => new KeyValuePair<string, int>(Enum.GetName(enumType, e), e)).ToList();
            }

            return null;
        }

        public static string SaveFile(string PathToSaveImage, string base64fileString, string extn)
        {
            try
            {
                if (base64fileString != null)
                {
                    string fileDirectory = TransportManagerSettings.ImagePathUrl + "\\Documents\\" + PathToSaveImage + "\\";
                    string relativeFilePath = "Documents\\" + PathToSaveImage + "\\";
                    if (!Directory.Exists(fileDirectory))
                    {
                        Directory.CreateDirectory(fileDirectory);
                    }
                    string fileName = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + extn;
                    string filePath = fileDirectory + fileName;

                    using (FileStream stream = File.Create(filePath))
                    {
                        byte[] byteArray = Convert.FromBase64String(base64fileString);
                        stream.Write(byteArray, 0, byteArray.Length);
                    }
                    string pathToReturn = (relativeFilePath + fileName).Replace('\\','/');
                    return pathToReturn;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return null;
        }
    }
}
